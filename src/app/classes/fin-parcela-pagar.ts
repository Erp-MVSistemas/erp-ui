import { FinParcelaPagamento } from './fin-parcela-pagamento';
import { ContaCaixa } from './conta-caixa';
import { FinLancamentoPagar } from './fin-lancamento-pagar';
import { FinStatusParcela } from './fin-status-parcela';

export class FinParcelaPagar {
id: number;
contaCaixa: ContaCaixa;
finLancamentoPagar: FinLancamentoPagar;
finStatusParcela: FinStatusParcela;
numeroParcela: number;
dataEmissao: string;
dataVencimento: string;
descontoAte: string;
valor: number;
listaFinParcelaPagamento: FinParcelaPagamento[];
}
