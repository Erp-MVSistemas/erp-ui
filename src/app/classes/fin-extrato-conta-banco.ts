import { ContaCaixa } from './conta-caixa';

export class FinExtratoContaBanco {
id: number;
contaCaixa: ContaCaixa;
mesAno: string;
mes: string;
ano: string;
dataMovimento: string;
dataBalancete: string;
historico: string;
documento: string;
valor: number;
conciliado: string;
observacao: string;
}
