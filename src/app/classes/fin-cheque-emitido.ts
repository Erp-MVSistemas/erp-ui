import { Cheque } from './cheque';

export class FinChequeEmitido {
id: number;
cheque: Cheque;
dataEmissao: string;
bomPara: string;
dataCompensacao: string;
valor: number;
nominalA: string;
}
