import { Estados } from './estados';
export class Cidades {
  id: number;
  codigoMunicipio: string;
  nome: string;
  estado: Estados = new Estados();
}
