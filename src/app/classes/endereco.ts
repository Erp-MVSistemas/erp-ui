import { Cidades } from './cidades';

export class Enderecos {
  endereco: Enderecos;
  bairro: string;
  cep: string;
  complemento: string;
  logradouro: string;
  numero: string;
  tipo: string;
  cidade: Cidades = new Cidades();
}
