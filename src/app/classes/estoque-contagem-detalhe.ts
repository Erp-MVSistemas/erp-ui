import { Produto } from './produto';
import { EstoqueContagemCabecalho } from './estoque-contagem-cabecalho';

export class EstoqueContagemDetalhe {
id: number;
estoqueContagemCabecalho: EstoqueContagemCabecalho;
produto: Produto;
quantidadeContada: number;
quantidadeSistema: number;
acuracidade: number;
divergencia: number;
}
