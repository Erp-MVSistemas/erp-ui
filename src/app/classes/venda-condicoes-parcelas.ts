import { VendaCondicoesPagamento } from './venda-condicoes-pagamento';

export class VendaCondicoesParcelas {
id: number;
vendaCondicoesPagamento: VendaCondicoesPagamento;
parcela: number;
dias: number;
taxa: number;
}
