import { RequisicaoInternaDetalhe } from './requisicao-interna-detalhe';
import { Colaborador } from './colaborador';

export class RequisicaoInternaCabecalho {
id: number;
colaborador: Colaborador;
dataRequisicao: string;
situacao: string;
listaRequisicaoInternaDetalhe: RequisicaoInternaDetalhe[];
}
