import { Pessoa } from './pessoa';

export class FinChequeRecebido {
id: number;
pessoa: Pessoa;
cpfCnpj: string;
nome: string;
codigoBanco: string;
codigoAgencia: string;
conta: string;
numero: number;
dataEmissao: string;
bomPara: string;
dataCompensacao: string;
valor: number;
}
