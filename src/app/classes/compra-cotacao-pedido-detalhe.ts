import { CompraPedido } from './compra-pedido';
import { CompraCotacaoDetalhe } from './compra-cotacao-detalhe';

export class CompraCotacaoPedidoDetalhe {
	id: number;
	compraPedido: CompraPedido;
	compraCotacaoDetalhe: CompraCotacaoDetalhe;
	quantidadePedida: number;
}
