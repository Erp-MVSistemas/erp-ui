import { ContaCaixa } from './conta-caixa';
import { FinParcelaPagar } from './fin-parcela-pagar';
import { Fornecedor } from './fornecedor';
import { FinNaturezaFinanceira } from './fin-natureza-financeira';
import { FinDocumentoOrigem } from './fin-documento-origem';

export class FinLancamentoPagar {
id: number;
finNaturezaFinanceira: FinNaturezaFinanceira;
finDocumentoOrigem: FinDocumentoOrigem;
fornecedor: Fornecedor;
quantidadeParcela: number;
valorTotal: number;
valorAPagar: number;
dataLancamento: string;
numeroDocumento: string;
imagemDocumento: string;
primeiroVencimento: string;
intervaloEntreParcelas: number;
listaFinParcelaPagar: FinParcelaPagar[];
contaCaixa: ContaCaixa;
}
