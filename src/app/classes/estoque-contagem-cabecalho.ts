import { EstoqueContagemDetalhe } from './estoque-contagem-detalhe';
export class EstoqueContagemCabecalho {
id: number;
dataContagem: string;
estoqueAtualizado: string;
listaEstoqueContagemDetalhe: EstoqueContagemDetalhe[];
}
