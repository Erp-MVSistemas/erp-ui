import { Produto } from './produto';
import { RequisicaoInternaCabecalho } from './requisicao-interna-cabecalho';

export class RequisicaoInternaDetalhe {
id: number;
reqInternaCabecalho: RequisicaoInternaCabecalho;
produto: Produto;
quantidade: number;
}
