import { Colaborador } from './colaborador';
import { EstoqueReajusteDetalhe } from './estoque-reajuste-detalhe';

export class EstoqueReajusteCabecalho {
id: number;
colaborador: Colaborador;
dataReajuste: string;
porcentagem: number;
tipoReajuste: string;
listaEstoqueReajusteDetalhe: EstoqueReajusteDetalhe[];
}
