import { CompraReqCotacaoDetalhe } from './compra-req-cotacao-detalhe';
import { CompraFornecedorCotacao } from './compra-fornecedor-cotacao';
export class CompraCotacao {
id: number;
dataCotacao: string;
descricao: string;
situacao: string;
listaCompraFornecedorCotacao: CompraFornecedorCotacao[] = [];
listaCompraReqCotacaoDetalhe: CompraReqCotacaoDetalhe[] = [];
}
