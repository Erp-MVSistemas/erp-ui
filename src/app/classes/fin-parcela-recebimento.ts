import { ContaCaixa } from './conta-caixa';
import { FinParcelaReceber } from './fin-parcela-receber';
import { FinTipoRecebimento } from './fin-tipo-recebimento';
import { FinChequeRecebido } from './fin-cheque-recebido';

export class FinParcelaRecebimento {
id: number;
finParcelaReceber: FinParcelaReceber;
finTipoRecebimento: FinTipoRecebimento;
finChequeRecebido: FinChequeRecebido;
contaCaixa: ContaCaixa;
dataRecebimento: string;
taxaJuro: number;
taxaMulta: number;
taxaDesconto: number;
valorJuro: number;
valorMulta: number;
valorDesconto: number;
valorRecebido: number;
historico: string;
}
