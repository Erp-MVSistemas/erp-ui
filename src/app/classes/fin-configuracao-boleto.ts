import { ContaCaixa } from './conta-caixa';

export class FinConfiguracaoBoleto {
id: number;
contaCaixa: ContaCaixa;
instrucao01: string;
instrucao02: string;
caminhoArquivoRemessa: string;
caminhoArquivoRetorno: string;
caminhoArquivoLogotipo: string;
caminhoArquivoPdf: string;
mensagem: string;
localPagamento: string;
layoutRemessa: string;
aceite: string;
especie: string;
carteira: string;
codigoConvenio: string;
codigoCedente: string;
taxaMulta: number;
}
