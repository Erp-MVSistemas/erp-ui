export class UnidadeProduto {
	id: number;
	sigla: string;
	descricao: string;
	podeFracionar: string;
}
