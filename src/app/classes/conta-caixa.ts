import { AgenciaBanco } from './agencia-banco';

export class ContaCaixa {
id: number;
agenciaBanco: AgenciaBanco;
codigo: string;
digito: string;
nome: string;
descricao: string;
tipo: string;
}
