import { Produto } from './produto';
import { VendaOrcamentoCabecalho } from './venda-orcamento-cabecalho';

export class VendaOrcamentoDetalhe {
id: number;
vendaOrcamentoCabecalho: VendaOrcamentoCabecalho;
produto: Produto;
quantidade: number;
valorUnitario: number;
valorSubtotal: number;
taxaDesconto: number;
valorDesconto: number;
valorTotal: number;
}
