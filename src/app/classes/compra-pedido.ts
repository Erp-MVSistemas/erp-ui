import { Fornecedor } from './fornecedor';
import { CompraTipoPedido } from './compra-tipo-pedido';

export class CompraPedido {
id: number;
compraTipoPedido: CompraTipoPedido;
fornecedor: Fornecedor;
dataPedido: string;
dataPrevistaEntrega: string;
dataPrevisaoPagamento: string;
localEntrega: string;
localCobranca: string;
contato: string;
valorSubtotal: number;
taxaDesconto: number;
valorDesconto: number;
valorTotalPedido: number;
tipoFrete: string;
formaPagamento: string;
quantidadeParcelas: number;
diasPrimeiroVencimento: number;
diasIntervalo: number;
}
