import { CompraFornecedorCotacao } from './compra-fornecedor-cotacao';
import { Produto } from './produto';

export class CompraCotacaoDetalhe {
	
	id: number;
	compraFornecedorCotacao: CompraFornecedorCotacao;
	produto: Produto;
	quantidade: number;
	quantidadePedida: number;
	valorUnitario: number;
	valorSubtotal: number;
	taxaDesconto: number;
	valorDesconto: number;
	valorTotal: number;
}
