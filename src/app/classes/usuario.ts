import { Papel } from './papel';
import { Colaborador } from './colaborador';
export class Usuario {
  usuario: Usuario;
  administrador: string;
  cpf: string;
  dataCadastro: string;
  selo: string;
  colaborador: Colaborador;
  papel: Papel;
  token: string;
}
