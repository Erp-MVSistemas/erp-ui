import { ContaCaixa } from './conta-caixa';

export class TalonarioCheque {
id: number;
contaCaixa: ContaCaixa;
talao: string;
numero: number;
statusTalao: string;
}
