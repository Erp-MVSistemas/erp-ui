import { CompraPedido } from './compra-pedido';
import { Produto } from './produto';

export class CompraPedidoDetalhe {
id: number;
compraPedido: CompraPedido;
produto: Produto;
quantidade: number;
valorUnitario: number;
valorSubtotal: number;
taxaDesconto: number;
valorDesconto: number;
valorTotal: number;
}
