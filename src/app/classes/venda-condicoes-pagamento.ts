import { VendaCondicoesParcelas } from './venda-condicoes-parcelas';
export class VendaCondicoesPagamento {
id: number;
nome: string;
descricao: string;
faturamentoMinimo: number;
faturamentoMaximo: number;
indiceCorrecao: number;
diasTolerancia: number;
valorTolerancia: number;
prazoMedio: number;
listaVendaCondicoesParcela: VendaCondicoesParcelas[] = [];
}
