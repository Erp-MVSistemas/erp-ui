import { ProdutoGrupo } from './produto-grupo';

export class ProdutoSubGrupo {
id: number;
grupo: ProdutoGrupo;
nome: string;
descricao: string;
}
