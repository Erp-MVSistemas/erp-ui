import { Pessoa } from './pessoa';
import { AtividadeForCli } from './atividade-for-cli';
import { SituacaoForCli } from './situacao-for-cli';

export class Cliente {
id: number;
pessoa: Pessoa;
atividadeForCli: AtividadeForCli;
situacaoForCli: SituacaoForCli;
desde: string;
dataCadastro: string;
observacao: string;
porcentoDesconto: number;
limiteCredito: number;
}
