import { ProdutoSubGrupo } from './produto-sub-grupo';
import { ProdutoMarca } from './produto-marca';
import { UnidadeProduto } from './unidade-produto';

export class Produto {
id: number;
unidadeProduto: UnidadeProduto;
produtoMarca: ProdutoMarca;
produtoSubGrupo: ProdutoSubGrupo;
gtin: string;
codigoInterno: string;
ncm: string;
nome: string;
descricao: string;
descricaoPdv: string;
valorCompra: number;
valorVenda: number;
precoVendaMinimo: number;
precoLucroZero: number;
precoLucroMinimo: number;
precoLucroMaximo: number;
quantidadeEstoque: number;
quantidadeEstoqueAnterior: number;
estoqueMinimo: number;
estoqueMaximo: number;
estoqueIdeal: number;
excluido: string;
inativo: string;
dataCadastro: string;
fotoProduto: string;
iat: string;
ippt: string;
tipoItemSped: string;
totalizadorParcial: string;
codigoBalanca: number;
}
