export class CompraTipoPedido {
	id: number;
	codigo: string;
	nome: string;
	descricao: string;
}
