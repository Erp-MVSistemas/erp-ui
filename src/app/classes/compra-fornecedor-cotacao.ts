import { Fornecedor } from './fornecedor';
import { CompraCotacao } from './compra-cotacao';

export class CompraFornecedorCotacao {
id: number;
compraCotacao: CompraCotacao;
fornecedor: Fornecedor;
prazoEntrega: string;
vendaCondicoesPagamento: string;
valorSubtotal: number;
taxaDesconto: number;
valorDesconto: number;
total: number;
}
