import { Empresa } from './empresa';
export class Armazem {
  id: number;
  nomeArmazem: string;
  empresa: Empresa;
}
