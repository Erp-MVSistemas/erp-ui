import { CompraCotacao } from './compra-cotacao';
import { CompraRequisicaoDetalhe } from './compra-requisicao-detalhe';

export class CompraReqCotacaoDetalhe {
	id: number;
	compraCotacao: CompraCotacao;
	compraRequisicaoDetalhe: CompraRequisicaoDetalhe;
	quantidadeCotada: number;
}
