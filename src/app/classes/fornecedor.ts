import { Pessoa } from './pessoa';
import { AtividadeForCli } from './atividade-for-cli';
import { SituacaoForCli } from './situacao-for-cli';

export class Fornecedor {
id: number;
pessoa: Pessoa;
atividadeForCli: AtividadeForCli;
situacaoForCli: SituacaoForCli;
desde: string;
optanteSimplesNacional: string;
localizacao: string;
dataCadastro: string;
sofreRetencao: string;
chequeNominalA: string;
observacao: string;
}
