import { ContaCaixa } from './conta-caixa';

export class FinFechamentoCaixaBanco {
id: number;
contaCaixa: ContaCaixa;
dataFechamento: string;
mesAno: string;
mes: string;
ano: string;
saldoAnterior: number;
recebimentos: number;
pagamentos: number;
saldoConta: number;
chequeNaoCompensado: number;
saldoDisponivel: number;
}
