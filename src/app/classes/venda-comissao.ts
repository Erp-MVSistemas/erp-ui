import { Vendedor } from './vendedor';
import { VendaCabecalho } from './venda-cabecalho';

export class VendaComissao {
id: number;
vendedor: Vendedor;
vendaCabecalho: VendaCabecalho;
valorVenda: number;
tipoContabil: string;
valorComissao: number;
situacao: string;
dataLancamento: string;
}
