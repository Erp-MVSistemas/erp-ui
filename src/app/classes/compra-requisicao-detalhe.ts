import { CompraRequisicao } from './compra-requisicao';
import { Produto } from './produto';

export class CompraRequisicaoDetalhe {
id: number;
compraRequisicao: CompraRequisicao;
produto: Produto;
quantidade: number;
quantidadeCotada: number;
itemCotado: string;
}
