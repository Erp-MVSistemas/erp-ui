import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Message } from 'primeng/api';
import { Router } from '@angular/router';
import { LoginService } from '../servicos/login.service';

@Injectable({
  providedIn: 'root'
})
export class VariaveisGlobais {
  tituloJanela = 'MvSistema - EtemoERP';
  enderecoUrl;
  msgs: Message[] = [];
  success = 'success';
  info = 'info';
  warn = 'warn';
  error = 'error';
  botaoDesabilitado: Boolean = true;
  loading: Boolean  = false;

  constructor(private router: Router,
    private loginService: LoginService) {
    this.tituloJanela = 'MvSistema - EtemoERP';
  }

  /**
   * @param tipo  = `success`, `info`, `warn`, `error`
   * @param titulo = `cabeçalho da mensagem`
   * @param mensagem = `corpo da mensagem`
   */
  mostraMensagem(tipo: string, titulo: string, mensagem: string) {
    this.msgs = [{ severity: tipo, summary: titulo, detail: mensagem }];
  }

  tratarError(error: HttpErrorResponse): string {

    if (error.status === 404) {
      this.router.navigate(['nao-encontrado']);
      this.tituloJanela = 'MvSistema - EtemoERP';
    }

    if (error.status === 401) {
      this.mostraMensagem(this.warn, 'Atenção!', 'Opps... ' + error.statusText);
      // this.loginService.logOut();
    }

    if (error.error) {
      return error.status + ' - ' + error.error.message;
    } else {
      return 'Error desconhecido';
    }
  }
}
