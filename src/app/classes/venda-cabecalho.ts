import { VendaDetalhe } from './venda-detalhe';
import { Vendedor } from './vendedor';
import { Cliente } from './cliente';
import { TipoNotaFiscal } from './tipo-nota-fiscal';
import { Transportadora } from './transportadora';
import { VendaOrcamentoCabecalho } from './venda-orcamento-cabecalho';
import { VendaCondicoesPagamento } from './venda-condicoes-pagamento';

export class VendaCabecalho {
id: number;
vendaOrcamentoCabecalho: VendaOrcamentoCabecalho;
vendaCondicoesPagamento: VendaCondicoesPagamento;
transportadora: Transportadora;
tipoNotaFiscal: TipoNotaFiscal;
cliente: Cliente;
vendedor: Vendedor;
dataVenda: string;
dataSaida: string;
horaSaida: string;
numeroFatura: number;
localEntrega: string;
localCobranca: string;
valorSubtotal: number;
taxaComissao: number;
valorComissao: number;
taxaDesconto: number;
valorDesconto: number;
valorTotal: number;
tipoFrete: string;
formaPagamento: string;
valorFrete: number;
valorSeguro: number;
observacao: string;
situacao: string;
listaVendaDetalhe: VendaDetalhe[] = [];
}
