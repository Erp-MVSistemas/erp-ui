import { Produto } from './produto';
import { VendaCabecalho } from './venda-cabecalho';

export class VendaDetalhe {
id: number;
produto: Produto;
vendaCabecalho: VendaCabecalho;
quantidade: number;
valorUnitario: number;
valorSubtotal: number;
taxaDesconto: number;
valorDesconto: number;
valorTotal: number;
taxaComissao: number;
valorComissao: number;
}
