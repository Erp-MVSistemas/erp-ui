import { FinParcelaRecebimento } from 'src/app/classes/fin-parcela-recebimento';
import { ContaCaixa } from './conta-caixa';
import { FinLancamentoReceber } from './fin-lancamento-receber';
import { FinStatusParcela } from './fin-status-parcela';

export class FinParcelaReceber {
id: number;
contaCaixa: ContaCaixa;
finLancamentoReceber: FinLancamentoReceber;
finStatusParcela: FinStatusParcela;
numeroParcela: number;
dataEmissao: string;
dataVencimento: string;
descontoAte: string;
valor: number;
emitiuBoleto: string;
boletoNossoNumero: string;
listaFinParcelaRecebimento: FinParcelaRecebimento[];
}
