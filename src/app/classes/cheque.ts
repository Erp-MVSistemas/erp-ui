import { TalonarioCheque } from './talonario-cheque';

export class Cheque {
id: number;
talonarioCheque: TalonarioCheque;
numero: number;
statusCheque: string;
dataStatus: string;
}
