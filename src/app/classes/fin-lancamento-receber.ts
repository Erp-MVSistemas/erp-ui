import { Cliente } from './cliente';
import { FinNaturezaFinanceira } from './fin-natureza-financeira';
import { FinDocumentoOrigem } from './fin-documento-origem';

export class FinLancamentoReceber {
id: number;
finNaturezaFinanceira: FinNaturezaFinanceira;
finDocumentoOrigem: FinDocumentoOrigem;
cliente: Cliente;
quantidadeParcela: number;
valorTotal: number;
valorAReceber: number;
dataLancamento: string;
numeroDocumento: string;
primeiroVencimento: string;
taxaComissao: number;
valorComissao: number;
intervaloEntreParcelas: number;
}
