import { TipoColaborador } from './tipo-colaborador';
import { Setor } from './setor';
import { Pessoa } from './pessoa';
import { Cargo } from './cargo';
export class Colaborador {
  id: number;
  ctpsDataExpedicao: string;
  ctpsNumero: string;
  ctpsSerie: string;
  ctpsUf: string;
  dataAdmissao: string;
  dataCadastro: string;
  dataDemissao: string;
  foto34: string;
  matricula: string;
  observacao: string;
  pagamentoAgencia: string;
  pagamentoAgenciaDigito: string;
  pagamentoBanco: string;
  pagamentoConta: string;
  pagamentoContaDigito: string;
  pagamentoForma: string;
  cargo: Cargo;
  pessoa: Pessoa;
  setor: Setor;
  tipoColaborador: TipoColaborador;
}
