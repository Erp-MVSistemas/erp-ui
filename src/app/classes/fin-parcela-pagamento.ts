import { ContaCaixa } from './conta-caixa';
import { FinParcelaPagar } from './fin-parcela-pagar';
import { FinChequeEmitido } from './fin-cheque-emitido';
import { FinTipoPagamento } from './fin-tipo-pagamento';

export class FinParcelaPagamento {
id: number;
finParcelaPagar: FinParcelaPagar;
finChequeEmitido: FinChequeEmitido;
finTipoPagamento: FinTipoPagamento;
contaCaixa: ContaCaixa;
dataPagamento: string;
taxaJuro: number;
taxaMulta: number;
taxaDesconto: number;
valorJuro: number;
valorMulta: number;
valorDesconto: number;
valorPago: number;
historico: string;
}
