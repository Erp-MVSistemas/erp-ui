import { Colaborador } from './colaborador';

export class Vendedor {
id: number;
colaborador: Colaborador;
comissao: number;
metaVendas: number;
}
