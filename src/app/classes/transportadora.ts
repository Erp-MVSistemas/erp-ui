import { Pessoa } from './pessoa';

export class Transportadora {
	id: number;
	pessoa: Pessoa;
	dataCadastro: string;
	observacao: string;
}
