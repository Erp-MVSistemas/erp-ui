export class TipoNotaFiscal {
id: number;
modelo: string;
serie: string;
nome: string;
descricao: string;
template: string;
numeroItens: number;
ultimoImpresso: number;
}
