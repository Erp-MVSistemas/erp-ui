import { Produto } from './produto';
import { EstoqueReajusteCabecalho } from './estoque-reajuste-cabecalho';

export class EstoqueReajusteDetalhe {
id: number;
estoqueReajusteCabecalho: EstoqueReajusteCabecalho;
produto: Produto;
valorOriginal: number;
valorReajuste: number;
}
