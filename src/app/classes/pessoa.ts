export class Pessoa {
  pessoa: Pessoa;
  cliente: string;
  colaborador: string;
  email: string;
  fornecedor: string;
  nome: string;
  site: string;
  tipo: string;
  transportadora: string;
}
