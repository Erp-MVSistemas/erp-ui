import { Telefone } from './telefone';
import { Enderecos } from './endereco';

export class Empresa {
  id: number;
  cnpj: string;
  email: string;
  ie: string;
  nomeFantasia: string;
  razaoSocial: string;
  tipoEmpresa: string;
  endereco: Enderecos = new Enderecos();
  telefones: Telefone[] = new Array<Telefone>();
  crt: string;
}
