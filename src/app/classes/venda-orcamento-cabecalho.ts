import { VendaOrcamentoDetalhe } from './venda-orcamento-detalhe';
import { Cliente } from './cliente';
import { Transportadora } from './transportadora';
import { Vendedor } from './vendedor';
import { VendaCondicoesPagamento } from './venda-condicoes-pagamento';

export class VendaOrcamentoCabecalho {
id: number;
vendaCondicoesPagamento: VendaCondicoesPagamento;
vendedor: Vendedor;
transportadora: Transportadora;
cliente: Cliente;
tipo: string;
codigo: string;
dataCadastro: string;
validade: string;
tipoFrete: string;
valorSubtotal: number;
valorFrete: number;
taxaComissao: number;
valorComissao: number;
taxaDesconto: number;
valorDesconto: number;
valorTotal: number;
observacao: string;
listaVendaOrcamentoDetalhe: VendaOrcamentoDetalhe[] = [];
}
