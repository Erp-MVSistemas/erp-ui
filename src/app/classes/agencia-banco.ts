import { Banco } from './banco';

export class AgenciaBanco {
id: number;
banco: Banco;
codigo: string;
digito: string;
nome: string;
logradouro: string;
numero: string;
cep: string;
bairro: string;
municipio: string;
uf: string;
telefone: string;
gerente: string;
contato: string;
observacao: string;
}
