import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { VendaCondicoesPagamento } from '../classes/venda-condicoes-pagamento';


@Injectable({
  providedIn: 'root'
})
export class VendaCondicoesPagamentoService {

  private endpoint = environment.enderecoUrl + 'vendas/condicoes-pagamento/';

  constructor(private http: HttpClient) {  }

  getListaVendaCondicoesPagamento(nome?: String): Observable<VendaCondicoesPagamento[]> {
    if (nome == null) {
      return this.http.get<VendaCondicoesPagamento[]>(this.endpoint);
    } else {
      return this.http.get<VendaCondicoesPagamento[]>(this.endpoint + 'lista/' + nome);
    }
  }

  getVendaCondicoesPagamento(id: number): Observable<VendaCondicoesPagamento> {
    return this.http.get<VendaCondicoesPagamento>(this.endpoint + id);
  }

  salvar(vendaCondicoesPagamento: VendaCondicoesPagamento): Observable<VendaCondicoesPagamento> {
    return this.http.post<VendaCondicoesPagamento>(this.endpoint, vendaCondicoesPagamento, environment.httpOpions);
  }

  excluir(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }

}
