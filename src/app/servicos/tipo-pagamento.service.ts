import { FinTipoPagamento } from './../classes/fin-tipo-pagamento';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TipoPagamentoService {

  private url = environment.enderecoUrl + 'financeiro/tipo-pagamento/';

  constructor(private http: HttpClient) { }

  getListaFinTipoPagamento(): Observable<FinTipoPagamento[]> {
      return this.http.get<FinTipoPagamento[]>(this.url);
  }

  getListaFinTipoPagamentoNome(nome: String): Observable<FinTipoPagamento[]> {
    if (nome == null) {
      return this.http.get<FinTipoPagamento[]>(this.url);
    } else {
      return this.http.get<FinTipoPagamento[]>(this.url + 'lista/' + nome);
    }
  }

  getFinTipoPagamento(id: number): Observable<FinTipoPagamento> {
    return this.http.get<FinTipoPagamento>(this.url + id);
  }

  salvar(tipoPagamento: FinTipoPagamento): Observable<FinTipoPagamento> {
    return this.http.post<FinTipoPagamento>(this.url, tipoPagamento, environment.httpOpions);
  }

  excluir(id: number): Observable<{}> {
    return this.http.delete(this.url + id);
  }

}
