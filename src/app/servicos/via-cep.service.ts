import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Address } from '../classes/address';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ViaCepService {

  constructor(private http: HttpClient) { }

  getBuscarCep(zipcode: string): Observable<Address> {
    return this.http.get<Address>(`https://viacep.com.br/ws/${zipcode}/json/`);
  }
}
