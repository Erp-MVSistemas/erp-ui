import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Colaborador } from '../classes/colaborador';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ColaboradorService {

  private colaboradores: Colaborador[];
  private endpoint = environment.enderecoUrl + 'colaborador/';

  constructor(private http: HttpClient) {
  }

  getColaboradores(): Observable<Colaborador[]> {
    return this.http.get<Colaborador[]>(this.endpoint);
  }

  getListaColaboradores(nome: string): Observable<Colaborador[]> {
    return this.http.get<Colaborador[]>(this.endpoint + 'lista/' + nome);
  }

  getColaborador(id: number): Observable<Colaborador> {
    return this.http.get<Colaborador>(this.endpoint + id);
  }
  getSalvar(colaborador: Colaborador): Observable<Colaborador> {
    return this.http.post<Colaborador>(this.endpoint, colaborador, environment.httpOpions);
  }

  getRemover(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }
}
