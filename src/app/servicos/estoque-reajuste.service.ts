import { EstoqueReajusteCabecalho } from './../classes/estoque-reajuste-cabecalho';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EstoqueReajusteService {

  private url = environment.enderecoUrl + 'estoque/reajuste/';

  constructor(private http: HttpClient) {  }

  getListaEstoqueReajusteCabecalho(): Observable<EstoqueReajusteCabecalho[]> {
    return this.http.get<EstoqueReajusteCabecalho[]>(this.url);
  }

  getEstoqueReajusteCabecalho(id: number): Observable<EstoqueReajusteCabecalho> {
    return this.http.get<EstoqueReajusteCabecalho>(this.url + id);
  }

  getSalvar(estoqueReajusteCabecalho: EstoqueReajusteCabecalho): Observable<EstoqueReajusteCabecalho> {
    return this.http.post<EstoqueReajusteCabecalho>(this.url, estoqueReajusteCabecalho, environment.httpOpions);
  }

  getExcluir(id: number): Observable<{}> {
    return this.http.delete(this.url + id);
  }

}
