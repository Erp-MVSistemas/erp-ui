import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Usuario } from '../classes/usuario';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private endpoint = environment.enderecoUrl + 'usuario';


  constructor(private http: HttpClient) { }

  getUsuario(cpf: string): Observable<Usuario> {
    return this.http.get<Usuario>(this.endpoint + '/cpf/' + cpf);
  }
}
