import { CompraRequisicaoDetalhe } from './../classes/compra-requisicao-detalhe';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
import { CompraRequisicao } from './../classes/compra-requisicao';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CompraRequisicaoService {

  private compraRequisicaoes: CompraRequisicao[];
  private endpoint = environment.enderecoUrl + 'compra/requisicao/';

  constructor(private http: HttpClient) {
  }

  getListaCompraRequisicao(): Observable<CompraRequisicao[]> {
    return this.http.get<CompraRequisicao[]>(this.endpoint);
  }

  getListaCompraRequisicaoDetalhe(): Observable<CompraRequisicaoDetalhe[]> {
    return this.http.get<CompraRequisicaoDetalhe[]>(this.endpoint + 'detalhe');
  }

  getCompraRequisicao(id: number): Observable<CompraRequisicao> {
    return this.http.get<CompraRequisicao>(this.endpoint + id);
  }
  getSalvar(compraRequisicao: CompraRequisicao): Observable<CompraRequisicao> {
    return this.http.post<CompraRequisicao>(this.endpoint, compraRequisicao, environment.httpOpions);
  }

  getRemover(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }
}
