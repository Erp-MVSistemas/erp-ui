import { FinNaturezaFinanceira } from './../classes/fin-natureza-financeira';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NaturezaFinanceiraService {

  private url = environment.enderecoUrl + 'financeiro/natureza-financeira/';

  constructor(private http: HttpClient) { }

  getListaFinNaturezaFinanceira(nome: String): Observable<FinNaturezaFinanceira[]> {
    if (nome == null) {
      return this.http.get<FinNaturezaFinanceira[]>(this.url);
    } else {
      return this.http.get<FinNaturezaFinanceira[]>(this.url + 'lista/' + nome);
    }
  }

  getFinNaturezaFinanceira(id: number): Observable<FinNaturezaFinanceira> {
    return this.http.get<FinNaturezaFinanceira>(this.url + id);
  }

  salvar(naturezaFinanceira: FinNaturezaFinanceira): Observable<FinNaturezaFinanceira> {
    return this.http.post<FinNaturezaFinanceira>(this.url, naturezaFinanceira, environment.httpOpions);
  }

  excluir(id: number): Observable<{}> {
    return this.http.delete(this.url + id);
  }

}
