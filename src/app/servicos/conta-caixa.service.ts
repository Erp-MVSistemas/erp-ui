import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ContaCaixa } from '../classes/conta-caixa';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContaCaixaService {

  private url = environment.enderecoUrl + 'conta-caixa/';

  constructor(private http: HttpClient) { }

  getListaContaCaixa(nome: String): Observable<ContaCaixa[]> {
    if (nome == null) {
      return this.http.get<ContaCaixa[]>(this.url);
    } else {
      return this.http.get<ContaCaixa[]>(this.url + 'lista/' + nome);
    }
  }

  getContaCaixa(id: number): Observable<ContaCaixa> {
    return this.http.get<ContaCaixa>(this.url + id);
  }

  salvar(contaCaixa: ContaCaixa): Observable<ContaCaixa> {
    return this.http.post<ContaCaixa>(this.url, contaCaixa, environment.httpOpions);
  }

  excluir(id: number): Observable<{}> {
    return this.http.delete(this.url + id);
  }

}
