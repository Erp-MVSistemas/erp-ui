import { EstoqueContagemCabecalho } from './../classes/estoque-contagem-cabecalho';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EstoqueContagemService {

  private url = environment.enderecoUrl + 'estoque/contagem/';

  constructor(private http: HttpClient) {  }

  getListaEstoqueContagemCabecalho(): Observable<EstoqueContagemCabecalho[]> {
    return this.http.get<EstoqueContagemCabecalho[]>(this.url);
  }

  getEstoqueContagemCabecalho(id: number): Observable<EstoqueContagemCabecalho> {
    return this.http.get<EstoqueContagemCabecalho>(this.url + id);
  }

  getSalvar(estoqueContagemCabecalho: EstoqueContagemCabecalho): Observable<EstoqueContagemCabecalho> {
    return this.http.post<EstoqueContagemCabecalho>(this.url, estoqueContagemCabecalho, environment.httpOpions);
  }

  getExcluir(id: number): Observable<{}> {
    return this.http.delete(this.url + id);
  }

}
