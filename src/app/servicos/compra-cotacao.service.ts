import { Injectable } from '@angular/core';
import { CompraCotacao } from '../classes/compra-cotacao';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompraCotacaoService {

  private compraCotacaoes: CompraCotacao[];
  private endpoint = environment.enderecoUrl + 'compra/cotacao/';

  constructor(private http: HttpClient) {
  }

  getCompraCotacoes(): Observable<CompraCotacao[]> {
    return this.http.get<CompraCotacao[]>(this.endpoint);
  }

  getCompraCotacao(id: number): Observable<CompraCotacao> {
    return this.http.get<CompraCotacao>(this.endpoint + id);
  }
  getSalvar(compraCotacao: CompraCotacao): Observable<CompraCotacao> {
    return this.http.post<CompraCotacao>(this.endpoint, compraCotacao, environment.httpOpions);
  }

  getRemover(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }
}
