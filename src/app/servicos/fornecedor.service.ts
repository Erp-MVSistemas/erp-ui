import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Fornecedor } from './../classes/fornecedor';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FornecedorService {

  private fornecedor: Fornecedor[];
  private endpoint = environment.enderecoUrl + 'fornecedor/';

  constructor(private http: HttpClient) {
  }

  getFornecedores(): Observable<Fornecedor[]> {
    return this.http.get<Fornecedor[]>(this.endpoint);
  }

  getListaFornecedores(nome: string): Observable<Fornecedor[]> {
    return this.http.get<Fornecedor[]>(this.endpoint + 'lista/' + nome);
  }

  getFornecedor(id: number): Observable<Fornecedor> {
    return this.http.get<Fornecedor>(this.endpoint + id);
  }
  getSalvar(fornecedor: Fornecedor): Observable<Fornecedor> {
    return this.http.post<Fornecedor>(this.endpoint, fornecedor, environment.httpOpions);
  }

  getRemover(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }
}
