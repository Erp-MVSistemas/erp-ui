import { Injectable } from '@angular/core';
import { TipoColaborador } from '../classes/tipo-colaborador';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TipoColaboradorService {

  private tipoColaboradores: TipoColaborador[];
  private endpoint = environment.enderecoUrl + 'tipocolaborador/';

  constructor(private http: HttpClient) {
  }

  getTipoColaboradores(): Observable<TipoColaborador[]> {
    return this.http.get<TipoColaborador[]>(this.endpoint);
  }

  getTipoColaborador(id: number): Observable<TipoColaborador> {
    return this.http.get<TipoColaborador>(this.endpoint + id);
  }

  getListaTipoColaboradores(nome: string): Observable<TipoColaborador[]> {
    return this.http.get<TipoColaborador[]>(this.endpoint + 'lista/' + nome);
  }
  getSalvar(tipoColaborador: TipoColaborador): Observable<TipoColaborador> {
    return this.http.post<TipoColaborador>(this.endpoint, tipoColaborador, environment.httpOpions);
  }

  getRemover(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }
}
