import { Empresa } from './../classes/empresa';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmpresaService {

  private empresa: Empresa[];
  private endpoint = environment.enderecoUrl + 'empresa/';

  constructor(private http: HttpClient) {
  }

  getEmpresas(): Observable<Empresa[]> {
    return this.http.get<Empresa[]>(this.endpoint);
  }

  getEmpresa(id: number): Observable<Empresa> {
    return this.http.get<Empresa>(this.endpoint + id);
  }

  getSalvar(empresa: Empresa): Observable<Empresa> {
    return this.http.post<Empresa>(this.endpoint, empresa, environment.httpOpions);
  }

  getRemover(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }
}
