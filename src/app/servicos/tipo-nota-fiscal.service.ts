import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { TipoNotaFiscal } from '../classes/tipo-nota-fiscal';


@Injectable({
  providedIn: 'root'
})
export class TipoNotaFiscalService {

  private endpoint = environment.enderecoUrl + 'vendas/tipo-nota-fiscal/';

  constructor(private http: HttpClient) {  }

  getListaTipoNotaFiscal(nome?: String): Observable<TipoNotaFiscal[]> {
    if (nome == null) {
      return this.http.get<TipoNotaFiscal[]>(this.endpoint);
    } else {
      return this.http.get<TipoNotaFiscal[]>(this.endpoint + 'lista/' + nome);
    }
  }

  getTipoNotaFiscal(id: number): Observable<TipoNotaFiscal> {
    return this.http.get<TipoNotaFiscal>(this.endpoint + id);
  }

  salvar(tipoNotaFiscal: TipoNotaFiscal): Observable<TipoNotaFiscal> {
    return this.http.post<TipoNotaFiscal>(this.endpoint, tipoNotaFiscal, environment.httpOpions);
  }

  excluir(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }

}
