import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { VendaOrcamentoCabecalho } from '../classes/venda-orcamento-cabecalho';

@Injectable({
  providedIn: 'root'
})
export class VendaOrcamentoService {

  private endpoint = environment.enderecoUrl + 'vendas/orcamento/';

  constructor(private http: HttpClient) {  }

  getListaVendaOrcamentoCabecalho(codigo?: String): Observable<VendaOrcamentoCabecalho[]> {
    if (codigo == null) {
      return this.http.get<VendaOrcamentoCabecalho[]>(this.endpoint);
    } else {
      return this.http.get<VendaOrcamentoCabecalho[]>(this.endpoint + 'lista/' + codigo);
    }
  }

  getVendaOrcamentoCabecalho(id: number): Observable<VendaOrcamentoCabecalho> {
    return this.http.get<VendaOrcamentoCabecalho>(this.endpoint + id);
  }

  salvar(vendaOrcamentoCabecalho: VendaOrcamentoCabecalho): Observable<VendaOrcamentoCabecalho> {
    return this.http.post<VendaOrcamentoCabecalho>(this.endpoint, vendaOrcamentoCabecalho, environment.httpOpions);
  }

  excluir(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }

}
