import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { FinLancamentoReceber } from '../classes/fin-lancamento-receber';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LancamentoReceberService {

  private url = environment.enderecoUrl + 'financeiro/lancamento-receber/';

  constructor(private http: HttpClient) { }

  getListaFinLancamentoReceber(): Observable<FinLancamentoReceber[]> {
      return this.http.get<FinLancamentoReceber[]>(this.url);
  }

  getFinLancamentoReceber(id: number): Observable<FinLancamentoReceber> {
    return this.http.get<FinLancamentoReceber>(this.url + id);
  }

  getSalvar(finLancamentoReceber: FinLancamentoReceber): Observable<FinLancamentoReceber> {
    return this.http.post<FinLancamentoReceber>(this.url, finLancamentoReceber, environment.httpOpions);
  }

  getExcluir(id: number): Observable<{}> {
    return this.http.delete(this.url + id);
  }
}
