import { Injectable } from '@angular/core';
import { Telefone } from '../classes/telefone';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TelefoneService {

  private telefones: Telefone[];

  constructor() {
    this.telefones = [];
  }

  getTelefones(): Telefone[] {
    for (let i = 0; i <= 2; i++) {
      const telefone = new Telefone();
      telefone.id = i;
      telefone.numero = '(84) 99999-999' + i;
      this.getTelefone(telefone);
    }
    return this.telefones;
  }

  getTelefone(telefone: Telefone) {
    return this.telefones.push(telefone);
  }
}
