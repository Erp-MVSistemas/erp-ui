import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { CompraTipoRequisicao } from '../classes/compra-tipo-requisicao';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CompraTipoRequisicaoService {

  private CompraTipoRequisicaoes: CompraTipoRequisicao[];
  private endpoint = environment.enderecoUrl + 'compra/tipo-requisicao/';

  constructor(private http: HttpClient) {
  }

  getCompraTipoRequisicaoes(): Observable<CompraTipoRequisicao[]> {
    return this.http.get<CompraTipoRequisicao[]>(this.endpoint);
  }

  getCompraTipoRequisicao(id: number): Observable<CompraTipoRequisicao[]> {
    return this.http.get<CompraTipoRequisicao[]>(this.endpoint + id);
  }

  getListaCompraTipoRequisicao(nome: string): Observable<CompraTipoRequisicao[]> {
    return this.http.get<CompraTipoRequisicao[]>(this.endpoint + 'lista/' + nome);
  }

  getSalvar(compraTipoRequisicao: CompraTipoRequisicao): Observable<CompraTipoRequisicao> {
    return this.http.post<CompraTipoRequisicao>(this.endpoint, compraTipoRequisicao, environment.httpOpions);
  }

  getRemover(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }
}
