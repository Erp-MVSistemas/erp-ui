import { environment } from 'src/environments/environment';
import { Cliente } from './../classes/cliente';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private clientes: Cliente[];
  private endpoint = environment.enderecoUrl + 'cliente/';

  constructor(private http: HttpClient) {
  }

  getClientes(): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.endpoint);
  }

  getListaClientes(nome: string): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.endpoint + 'lista/' + nome);
  }

  getCliente(id: number): Observable<Cliente> {
    return this.http.get<Cliente>(this.endpoint + id);
  }
  getSalvar(cliente: Cliente): Observable<Cliente> {
    return this.http.post<Cliente>(this.endpoint, cliente, environment.httpOpions);
  }

  getRemover(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }
}
