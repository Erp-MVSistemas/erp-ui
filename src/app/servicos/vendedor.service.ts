import { environment } from './../../environments/environment';
import { Vendedor } from './../classes/vendedor';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VendedorService {

  private endpoint = environment.enderecoUrl + 'vendedor/';

  constructor(private http: HttpClient) {  }

  getListaVendedor(codigo?: String): Observable<Vendedor[]> {
    if (codigo == null) {
      return this.http.get<Vendedor[]>(this.endpoint);
    } else {
      return this.http.get<Vendedor[]>(this.endpoint + 'lista/' + codigo);
    }
  }

  getVendedor(id: number): Observable<Vendedor> {
    return this.http.get<Vendedor>(this.endpoint + id);
  }

  salvar(vendedor: Vendedor): Observable<Vendedor> {
    return this.http.post<Vendedor>(this.endpoint, vendedor, environment.httpOpions);
  }

  excluir(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }

}
