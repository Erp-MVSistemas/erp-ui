import { FinParcelaPagar } from './../classes/fin-parcela-pagar';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ParcelaPagamentoService {

  private url = environment.enderecoUrl + 'financeiro/parcela-pagamento/';

  constructor(private http: HttpClient) { }

  getListaFinParcelaPagar(): Observable<FinParcelaPagar[]> {
      return this.http.get<FinParcelaPagar[]>(this.url);
  }

  getFinParcelaPagar(id: number): Observable<FinParcelaPagar> {
    return this.http.get<FinParcelaPagar>(this.url + id);
  }

  getSalvar(parcelaPagamento: FinParcelaPagar): Observable<FinParcelaPagar> {
    return this.http.post<FinParcelaPagar>(this.url, parcelaPagamento, environment.httpOpions);
  }

  getExcluir(id: number): Observable<{}> {
    return this.http.delete(this.url + id);
  }

}
