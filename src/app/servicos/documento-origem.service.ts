import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FinDocumentoOrigem } from '../classes/fin-documento-origem';

@Injectable({
  providedIn: 'root'
})
export class DocumentoOrigemService {

  private url = environment.enderecoUrl + 'financeiro/documento-origem/';

  constructor(private http: HttpClient) { }

  getListaFinDocumentoOrigem(nome: String): Observable<FinDocumentoOrigem[]> {
    if (nome == null) {
      return this.http.get<FinDocumentoOrigem[]>(this.url);
    } else {
      return this.http.get<FinDocumentoOrigem[]>(this.url + 'lista/' + nome);
    }
  }

  getFinDocumentoOrigem(id: number): Observable<FinDocumentoOrigem> {
    return this.http.get<FinDocumentoOrigem>(this.url + id);
  }

  salvar(documentoOrigem: FinDocumentoOrigem): Observable<FinDocumentoOrigem> {
    return this.http.post<FinDocumentoOrigem>(this.url, documentoOrigem, environment.httpOpions);
  }

  excluir(id: number): Observable<{}> {
    return this.http.delete(this.url + id);
  }

}
