import { FinLancamentoPagar } from './../classes/fin-lancamento-pagar';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LancamentoPagarService {

  private url = environment.enderecoUrl + 'financeiro/lancamento-pagar/';

  constructor(private http: HttpClient) { }

  getListaFinLancamentoPagar(): Observable<FinLancamentoPagar[]> {
      return this.http.get<FinLancamentoPagar[]>(this.url);
  }

  getFinLancamentoPagar(id: number): Observable<FinLancamentoPagar> {
    return this.http.get<FinLancamentoPagar>(this.url + id);
  }

  getSalvar(finLancamentoPagar: FinLancamentoPagar): Observable<FinLancamentoPagar> {
    return this.http.post<FinLancamentoPagar>(this.url, finLancamentoPagar, environment.httpOpions);
  }

  getExcluir(id: number): Observable<{}> {
    return this.http.delete(this.url + id);
  }

}
