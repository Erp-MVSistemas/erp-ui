import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FinTipoRecebimento } from '../classes/fin-tipo-recebimento';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TipoRecebimentoService {

  private url = environment.enderecoUrl + 'financeiro/tipo-recebimento/';

  constructor(private http: HttpClient) { }

  getListaFinTipoRecebimento(): Observable<FinTipoRecebimento[]> {
      return this.http.get<FinTipoRecebimento[]>(this.url);
  }

  getListaFinTipoRecebimentoNome(nome: String): Observable<FinTipoRecebimento[]> {
    if (nome == null) {
      return this.http.get<FinTipoRecebimento[]>(this.url);
    } else {
      return this.http.get<FinTipoRecebimento[]>(this.url + 'lista/' + nome);
    }
  }

  getFinTipoRecebimento(id: number): Observable<FinTipoRecebimento> {
    return this.http.get<FinTipoRecebimento>(this.url + id);
  }

  salvar(tipoRecebimento: FinTipoRecebimento): Observable<FinTipoRecebimento> {
    return this.http.post<FinTipoRecebimento>(this.url, tipoRecebimento, environment.httpOpions);
  }

  excluir(id: number): Observable<{}> {
    return this.http.delete(this.url + id);
  }
}
