import { environment } from './../../environments/environment';
import { MessageService } from 'primeng/components/common/messageservice';
import { Armazem } from './../classes/armazem';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArmazemService {

  private armazens: Armazem[];
  private endpoint = environment.enderecoUrl + 'armazem/';

  constructor(private http: HttpClient) {
  }

  getArmazens(): Observable<Armazem[]> {
    return this.http.get<Armazem[]>(this.endpoint);
  }

  getArmazem(id: number): Observable<Armazem> {
    return this.http.get<Armazem>(this.endpoint + '/' + id);
  }
  getSalvar(armazem: Armazem): Observable<Armazem> {
    return this.http.post<Armazem>(this.endpoint, armazem, environment.httpOpions);
  }

  getRemover(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }
}
