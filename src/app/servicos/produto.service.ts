import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Produto } from './../classes/produto';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  private produtos: Produto[];
  private endpoint = environment.enderecoUrl + 'produto/';

  constructor(private http: HttpClient) {
  }

  getProdutos(): Observable<Produto[]> {
    return this.http.get<Produto[]>(this.endpoint);
  }

  getListaProdutos(nome: string): Observable<Produto[]> {
    return this.http.get<Produto[]>(this.endpoint + 'lista/' + nome);
  }

  getProduto(id: number): Observable<Produto> {
    return this.http.get<Produto>(this.endpoint + id);
  }
  getSalvar(produto: Produto): Observable<Produto> {
    return this.http.post<Produto>(this.endpoint, produto, environment.httpOpions);
  }

  getRemover(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }
}
