import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { FinParcelaReceber } from '../classes/fin-parcela-receber';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ParcelaRecebimentoService {

  private url = environment.enderecoUrl + 'financeiro/parcela-recebimento/';

  constructor(private http: HttpClient) { }

  getListaFinParcelaReceber(): Observable<FinParcelaReceber[]> {
      return this.http.get<FinParcelaReceber[]>(this.url);
  }

  getFinParcelaReceber(id: number): Observable<FinParcelaReceber> {
    return this.http.get<FinParcelaReceber>(this.url + id);
  }

  getSalvar(parcelaRecebimento: FinParcelaReceber): Observable<FinParcelaReceber> {
    return this.http.post<FinParcelaReceber>(this.url, parcelaRecebimento, environment.httpOpions);
  }

  getExcluir(id: number): Observable<{}> {
    return this.http.delete(this.url + id);
  }
}
