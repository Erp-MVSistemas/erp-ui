import { FinStatusParcela } from './../classes/fin-status-parcela';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StatusParcelaService {

  private url = environment.enderecoUrl + 'financeiro/status-parcela/';

  constructor(private http: HttpClient) { }

  getListaFinStatusParcela(nome: String): Observable<FinStatusParcela[]> {
    if (nome == null) {
      return this.http.get<FinStatusParcela[]>(this.url);
    } else {
      return this.http.get<FinStatusParcela[]>(this.url + 'lista/' + nome);
    }
  }

  getFinStatusParcela(id: number): Observable<FinStatusParcela> {
    return this.http.get<FinStatusParcela>(this.url + id);
  }

  salvar(statusParcela: FinStatusParcela): Observable<FinStatusParcela> {
    return this.http.post<FinStatusParcela>(this.url, statusParcela, environment.httpOpions);
  }

  excluir(id: number): Observable<{}> {
    return this.http.delete(this.url + id);
  }

}
