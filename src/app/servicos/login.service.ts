import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Usuario } from '../classes/usuario';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { Http, Headers } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url = environment.enderecoUrl + 'login';
  usuario: Usuario;
  dataExpiracao: Date;
  dataHoje: Date;
  intervalo: any;
  tempoAcesso: any;
  horaReversa: any;
  oauthToken = 'oauth/token';
  jwtPayload: any;
  enderecoUrl = environment.enderecoUrl + this.oauthToken;
  interval: any;

  constructor(private httpCli: HttpClient,
    private http: Http,
    private rota: Router,
    private helper: JwtHelperService) {
    this.usuario = new Usuario();
    this.dataExpiracao = new Date(0);
    this.tempoAcesso = 0;
    this.interval = 0;
    this.carregaToken();
  }

  /**
   * Metodo que valida as permissoes dos usuarios
   * @param permissao
   */
  temPermissao(permissao: string) {
      return this.jwtPayload && this.jwtPayload.authorities.includes(permissao);
  }

  obterNovoAccessToken(): Promise<void> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Authorization', 'Basic YW5ndWxhcjpAbmd1bEByMA==');
    const body = 'grant_type=refresh_token';

    return this.http.post(this.enderecoUrl, body, { headers,  withCredentials: true })
    .toPromise()
    .then(response => {
      console.log('Novo Access Token criado!');
      this.armazenraToken(response.json().access_token);
      return Promise.resolve(null);
    })
    .catch(response => {
        console.error('Erro ao renovar token: ' + response);
        return Promise.resolve(null);
    });
  }

  isAccessTokenInvalido() {
    const token = localStorage.getItem('token');
    return token || this.helper.isTokenExpired(token);
  }

  /**
   * Novo Metodo de Login usando oauth2 e jwt
   * @param usuario
   */
  loginIn(usuario: Usuario): Promise<void> {
    usuario.cpf = usuario.cpf.replace(/[^\d]+/g, '');
    const headers = new Headers();
    const body = `username=${usuario.cpf}&password=${usuario.selo}&grant_type=password`;

    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Authorization', 'Basic YW5ndWxhcjpAbmd1bEByMA==');

    return this.http.post(this.enderecoUrl, body, { headers, withCredentials: true })
      .toPromise()
      .then(response => {
        this.armazenraToken(response.json().access_token);
        this.tempoDeAcesso(response.json().access_token);
      })
      .catch(response => {
        if (response.status === 400) {
          const resposejson = response.json();

          if (resposejson.error === 'invalid_grant') {
            return Promise.reject('Cpf e/ou Senha invalidos!');
          }
          return Promise.reject(response);
        }

        if (response.status === 0) {
          return Promise.reject('Erro de Servidor desconectado!');
        }

        return Promise.reject(response);
      });
  }

  private armazenraToken(token: string) {
    this.jwtPayload = this.helper.decodeToken(token);
    localStorage.setItem('token', token);
  }

  private carregaToken() {
    const token = localStorage.getItem('token');
    if (token) {
      this.armazenraToken(token);
    }
  }

  logOut() {
    localStorage.removeItem('token');
    localStorage.removeItem('tempoExpira');
    clearInterval(this.interval);
    this.rota.navigate(['/login']);
  }

  tempoDeAcesso(token: string) {
    this.dataExpiracao  = this.helper.getTokenExpirationDate(token);

        const hora = moment(this.dataExpiracao, 'HH:mm:ss');

        const horaDif = moment(this.dataExpiracao, 'DD/MM/YYYY HH:mm:ss')
          .diff(moment(new Date(), 'DD/MM/YYYY HH:mm:ss'));
        const d = moment.duration(horaDif);
        this.intervalo = Math.floor(d.asSeconds());

        this.interval = setInterval(() => {
          this.dataHoje = new Date();

          if (!hora.isBefore(this.dataHoje)) {
            const time = this.intervalo -= 1;
            this.tempoAcesso = this.transforma_magicamente(time);
          } else {
            this.logOut();
          }
        }, 1000);
  }

  getDataExpiracaoToken(tempo: number): Date {
    const date = new Date(0);
    date.setUTCSeconds(tempo);
    return date;
  }

  getUsuarioLogado() {
    if (localStorage.getItem('token') != null) {
      return true;
    }
    return false;
  }

  private zeroAEsquerda(str, length) {
    const resto = length - String(str).length;
    return '0'.repeat(resto > 0 ? resto : 0) + str;
  }

  private horaFormatada(hora: Date) {
    const horaAtual = hora.getHours();
    const minutoAtual = hora.getMinutes();
    const segundoAtual = this.zeroAEsquerda(hora.getSeconds(), 2);
    return horaAtual + ':'
      + minutoAtual + ':'
      + segundoAtual;
  }
  public transforma_magicamente(s: number) {

    const hora = this.duas_casas(Math.round(s / 3600));
    const minuto = this.duas_casas(Math.floor((s % 3600) / 60));
    const segundo = this.duas_casas((s % 3600) % 60);

    const formatado = hora + ':' + minuto + ':' + segundo;

    return formatado;
  }

  private duas_casas(numero) {
    if (numero <= 9) {
      numero = '0' + numero;
    }
    return numero;
  }
}
