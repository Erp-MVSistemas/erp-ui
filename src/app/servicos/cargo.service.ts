import { Cargo } from './../classes/cargo';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CargoService {

  private cargos: Cargo[];
  private endpoint = environment.enderecoUrl + 'cargo/';

  constructor(private http: HttpClient) {
  }

  getCargos(): Observable<Cargo[]> {
    return this.http.get<Cargo[]>(this.endpoint);
  }

  getListaCargos(nome: string): Observable<Cargo[]> {
    return this.http.get<Cargo[]>(this.endpoint + 'lista/' + nome);
  }

  getCargo(id: number): Observable<Cargo> {
    return this.http.get<Cargo>(this.endpoint + id);
  }
  getSalvar(cargo: Cargo): Observable<Cargo> {
    return this.http.post<Cargo>(this.endpoint, cargo, environment.httpOpions);
  }

  getRemover(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }
}
