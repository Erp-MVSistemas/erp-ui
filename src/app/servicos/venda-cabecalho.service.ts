import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { VendaCabecalho } from '../classes/venda-cabecalho';


@Injectable({
  providedIn: 'root'
})
export class VendaCabecalhoService {

  private endpoint = environment.enderecoUrl + 'vendas/pedido/';

  constructor(private http: HttpClient) {  }

  getListaVendaCabecalho(): Observable<VendaCabecalho[]> {
      return this.http.get<VendaCabecalho[]>(this.endpoint);
  }

  getVendaCabecalho(id: number): Observable<VendaCabecalho> {
    return this.http.get<VendaCabecalho>(this.endpoint + id);
  }

  salvar(vendaCabecalho: VendaCabecalho): Observable<VendaCabecalho> {
    return this.http.post<VendaCabecalho>(this.endpoint, vendaCabecalho, environment.httpOpions);
  }

  excluir(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }

}
