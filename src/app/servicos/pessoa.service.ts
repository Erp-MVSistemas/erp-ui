import { Pessoa } from './../classes/pessoa';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PessoaService {

  private pessoas: Pessoa[];
  private endpoint = environment.enderecoUrl + 'pessoa/';

  constructor(private http: HttpClient) {
  }

  getPessoas(): Observable<Pessoa[]> {
    return this.http.get<Pessoa[]>(this.endpoint);
  }

  getListaPessoas(nome: String): Observable<Pessoa[]> {
    return this.http.get<Pessoa[]>(this.endpoint + 'lista/' + nome);
  }

  getPessoa(id: number): Observable<Pessoa> {
    return this.http.get<Pessoa>(this.endpoint + id);
  }
  getSalvar(pessoa: Pessoa): Observable<Pessoa> {
    return this.http.post<Pessoa>(this.endpoint, pessoa, environment.httpOpions);
  }

  getRemover(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }
}
