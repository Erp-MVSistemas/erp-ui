import { Injectable } from '@angular/core';
import { Transportadora } from '../classes/transportadora';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransportadoraService {

  private transportadora: Transportadora[];
  private endpoint = environment.enderecoUrl + 'transportadora/';

  constructor(private http: HttpClient) {
  }

  getTransportadoras(): Observable<Transportadora[]> {
    return this.http.get<Transportadora[]>(this.endpoint);
  }

  getListaTransportadoras(nome: string): Observable<Transportadora[]> {
    return this.http.get<Transportadora[]>(this.endpoint + 'lista/' + nome);
  }

  getTransportadora(id: number): Observable<Transportadora> {
    return this.http.get<Transportadora>(this.endpoint + id);
  }
  getSalvar(transportadora: Transportadora): Observable<Transportadora> {
    return this.http.post<Transportadora>(this.endpoint, transportadora, environment.httpOpions);
  }

  getRemover(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }
}
