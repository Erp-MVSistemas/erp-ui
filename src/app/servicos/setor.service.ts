import { Injectable } from '@angular/core';
import { Setor } from '../classes/setor';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SetorService {

  private setores: Setor[];
  private endpoint = environment.enderecoUrl + 'setor/';

  constructor(private http: HttpClient) {
  }

  getSetores(): Observable<Setor[]> {
    return this.http.get<Setor[]>(this.endpoint);
  }

  getListaSetores(nome: String): Observable<Setor[]> {
    return this.http.get<Setor[]>(this.endpoint + 'lista/' + nome);
  }

  getSetor(id: number): Observable<Setor> {
    return this.http.get<Setor>(this.endpoint + id);
  }
  getSalvar(setor: Setor): Observable<Setor> {
    return this.http.post<Setor>(this.endpoint, setor, environment.httpOpions);
  }

  getRemover(id: number): Observable<{}> {
    return this.http.delete(this.endpoint + id);
  }
}
