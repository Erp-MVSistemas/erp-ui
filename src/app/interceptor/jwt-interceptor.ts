import { LoginService } from './../servicos/login.service';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as moment from 'moment';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  dataExpiracao: Date;
  dataHoje: Date;
  intervalo: any;
  horaReversa: any;

  constructor(private loginService: LoginService,
    private helper: JwtHelperService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.loginService.obterNovoAccessToken()
      .then(() => {
        clearInterval(this.loginService.interval);
        this.loginService.tempoDeAcesso(localStorage.getItem('token'));
      });
    // const usuario: Usuario = JSON.parse(localStorage.getItem('token'));
    const urlViaCep = 'https://viacep.com.br/';

    if (localStorage.getItem('token')) {
      if ((urlViaCep.localeCompare(req.url) !== -1)) {
        req = req.clone({
          setHeaders: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        });
      }
    }

    return next.handle(req);
  }

}
