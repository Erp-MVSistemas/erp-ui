import { Usuario } from './classes/usuario';
import { LoginService } from './servicos/login.service';
import { VariaveisGlobais } from './classes/variaveis-globais';
import { Component, OnInit } from '@angular/core';

import { MenuItem } from 'primeng/api';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  items: MenuItem[];
  mostrarMenu: boolean;
  usuarioLogado: Usuario;
  usuarioCpf: Usuario;

  constructor(private global: VariaveisGlobais,
    private loginService: LoginService) {
      this.global.tituloJanela = 'MvSistema - EtemoERP';
    this.mostrarMenu = false;
  }

  ngOnInit() {
    this.global.tituloJanela = 'MvSistema - EtemoERP';
    this.items = [
      { label: 'Dashboard', icon: 'pi pi-fw pi-home', routerLink: ['/'] },
      {
        label: 'Cadastros',
        icon: 'pi pi-fw pi-pencil',
        items: [
          { label: 'Cargo', icon: 'pi pi-fw pi-upload', routerLink: ['/cargo'] },
          { label: 'Empresa', icon: 'pi pi-fw pi-upload', routerLink: ['/empresa'] },
          { label: 'Armazem', icon: 'pi pi-fw pi-upload', routerLink: ['/armazem'] }
        ]
      }
    ];
  }
}
