import { ParcelaRecebimentoDetalheComponent } from './pages/financeiro/parcela-recebimento-detalhe/parcela-recebimento-detalhe.component';
import { ParcelaRecebimentoComponent } from './pages/financeiro/parcela-recebimento/parcela-recebimento.component';
import { LancamentoReceberDetalheComponent } from './pages/financeiro/lancamento-receber-detalhe/lancamento-receber-detalhe.component';
import { LancamentoReceberComponent } from './pages/financeiro/lancamento-receber/lancamento-receber.component';
import { ParcelaPagamentoComponent } from './pages/financeiro/parcela-pagamento/parcela-pagamento.component';
import { ContagemDetalheComponent } from './pages/estoque/contagem-detalhe/contagem-detalhe.component';
import { ReajusteDetalheComponent } from './pages/estoque/reajuste-detalhe/reajuste-detalhe.component';
import { PedidoDetalheComponent } from './pages/vendas/pedido-detalhe/pedido-detalhe.component';
import { PedidoComponent } from './pages/vendas/pedido/pedido.component';
import { OrcamentoDetalheComponent } from './pages/vendas/orcamento-detalhe/orcamento-detalhe.component';
import { OrcamentoComponent } from './pages/vendas/orcamento/orcamento.component';
import { CompraCotacaoDetalheComponent } from './pages/compras/compra-cotacao-detalhe/compra-cotacao-detalhe.component';
import { CompraCotacaoComponent } from './pages/compras/compra-cotacao/compra-cotacao.component';
import { ColaboradorDetalheComponent } from './pages/cadastros/colaborador-detalhe/colaborador-detalhe.component';
import { ColaboradorComponent } from './pages/cadastros/colaborador/colaborador.component';
import { EmpresaDetalheComponent } from './pages/cadastros/empresa-detalhe/empresa-detalhe.component';
import { EmpresaComponent } from './pages/cadastros/empresa/empresa.component';
import { AuthGuardService } from './auth-guard.service';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { NaoEncontradoComponent } from './pages/error/nao-encontrado/nao-encontrado.component';
import { ArmazemDetalheComponent } from './pages/cadastros/armazem-detalhe/armazem-detalhe.component';
import { ArmazemComponent } from './pages/cadastros/armazem/armazem.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CargoComponent } from './pages/cadastros/cargo/cargo.component';
import { CargoDetalheComponent } from './pages/cadastros/cargo-detalhe/cargo-detalhe.component';
import { CompraRequisicaoDetalheComponent } from './pages/compras/compra-requisicao-detalhe/compra-requisicao-detalhe.component';
import { CompraRequisicaoComponent } from './pages/compras/compra-requisicao/compra-requisicao.component';
import { ReajusteComponent } from './pages/estoque/reajuste/reajuste.component';
import { ContagemComponent } from './pages/estoque/contagem/contagem.component';
import { LancamentoPagarComponent } from './pages/financeiro/lancamento-pagar/lancamento-pagar.component';
import { LancamentoPagarDetalheComponent } from './pages/financeiro/lancamento-pagar-detalhe/lancamento-pagar-detalhe.component';
import { ParcelaPagamentoDetalheComponent } from './pages/financeiro/parcela-pagamento-detalhe/parcela-pagamento-detalhe.component';

const routes: Routes = [
  {
    path: '', canActivate: [AuthGuardService],
    children: [
      { path: '', component: HomeComponent },
      { path: 'cargo', component: CargoComponent },
      { path: 'cargo/novo', component: CargoDetalheComponent },
      { path: 'cargo/:id', component: CargoDetalheComponent },
      { path: 'colaborador', component: ColaboradorComponent },
      { path: 'colaborador/novo', component: ColaboradorDetalheComponent },
      { path: 'colaborador/:id', component: ColaboradorDetalheComponent },
      { path: 'armazem', component: ArmazemComponent },
      { path: 'armazem/novo', component: ArmazemDetalheComponent },
      { path: 'armazem/:id', component: ArmazemDetalheComponent },
      { path: 'compra-requisicao', component: CompraRequisicaoComponent },
      { path: 'compra-requisicao/novo', component: CompraRequisicaoDetalheComponent },
      { path: 'compra-requisicao/:id', component: CompraRequisicaoDetalheComponent },
      { path: 'compra-cotacao', component: CompraCotacaoComponent },
      { path: 'compra-cotacao/novo', component: CompraCotacaoDetalheComponent},
      { path: 'compra-cotacao/:id', component: CompraCotacaoDetalheComponent },
      { path: 'empresa', component: EmpresaComponent },
      { path: 'empresa/novo', component: EmpresaDetalheComponent },
      { path: 'emrpesa/:id', component: EmpresaDetalheComponent },
      { path: 'venda-orcamento', component: OrcamentoComponent},
      { path: 'venda-orcamento/novo', component: OrcamentoDetalheComponent },
      { path: 'venda-orcamento/:id', component: OrcamentoDetalheComponent },
      { path: 'venda-pedido', component: PedidoComponent},
      { path: 'venda-pedido/novo', component: PedidoDetalheComponent },
      { path: 'venda-pedido/:id', component: PedidoDetalheComponent },
      { path: 'estoque-reajuste', component: ReajusteComponent},
      { path: 'estoque-reajuste/novo', component: ReajusteDetalheComponent },
      { path: 'estoque-reajuste/:id', component: ReajusteDetalheComponent },
      { path: 'estoque-contagem', component: ContagemComponent},
      { path: 'estoque-contagem/novo', component: ContagemDetalheComponent },
      { path: 'estoque-contagem/:id', component: ContagemDetalheComponent },
      { path: 'lancamento-pagar', component: LancamentoPagarComponent},
      { path: 'lancamento-pagar/novo', component: LancamentoPagarDetalheComponent },
      { path: 'lancamento-pagar/:id', component: LancamentoPagarDetalheComponent },
      { path: 'parcela-pagamento', component: ParcelaPagamentoComponent},
      { path: 'parcela-pagamento/novo', component: ParcelaPagamentoDetalheComponent },
      { path: 'parcela-pagamento/:id', component: ParcelaPagamentoDetalheComponent },
      { path: 'lancamento-receber', component: LancamentoReceberComponent},
      { path: 'lancamento-receber/novo', component: LancamentoReceberDetalheComponent},
      { path: 'lancamento-receber/:id', component: LancamentoPagarDetalheComponent},
      { path: 'parcela-recebimento', component: ParcelaRecebimentoComponent},
      { path: 'parcela-recebimento/novo', component: ParcelaRecebimentoDetalheComponent },
      { path: 'parcela-recebimento/:id', component: ParcelaRecebimentoDetalheComponent },
      { path: 'nao-encontrado', component: NaoEncontradoComponent },
    ]
  },
  { path: 'login', component: LoginComponent }
];
@NgModule({
  exports: [RouterModule],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
