import { VariaveisGlobais } from './../../../classes/variaveis-globais';

import { EmpresaService } from './../../../servicos/empresa.service';
import { ArmazemService } from './../../../servicos/armazem.service';

import { Armazem } from './../../../classes/armazem';
import { Empresa } from '../../../classes/empresa';

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-armazem-detalhe',
  templateUrl: './armazem-detalhe.component.html',
  styleUrls: ['./armazem-detalhe.component.css']
})
export class ArmazemDetalheComponent implements OnInit {

  armazem: Armazem;
  empresas: Empresa[];

  constructor(private armazemService: ArmazemService,
    private route: ActivatedRoute,
    private router: Router,
    private locate: Location,
    private empresaService: EmpresaService,
    private global: VariaveisGlobais) {
      this.global.tituloJanela = '';
    this.armazem = new Armazem();
  }

  ngOnInit() {
    this.empresaService.getEmpresas().subscribe(
      obj => {
        this.empresas = obj;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );

    if (this.router.url === '/armazem/novo') {
      this.global.tituloJanela = 'Adiconar Armazem';
      this.armazem = new Armazem();
    } else {
      const id = this.route.snapshot.paramMap.get('id');
      this.armazemService.getArmazem(parseInt(id, 0)).subscribe(
        dados => {
          this.armazem = dados;
        },
        error => {
          this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
        }
      );
      this.global.tituloJanela = 'Editando Armazem';
      this.empresas.indexOf(this.armazem.empresa);
    }
  }

  cancelar() {
    this.locate.back();
  }

  salvar() {
    this.armazemService.getSalvar(this.armazem).subscribe(
      dados => {
        this.armazem = dados;
        this.global.mostraMensagem(this.global.info, 'Sucesso!',
          this.armazem == null ? 'Armazem salvo com Sucesso' : 'Armazem alterado com Sucesso');
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );

  }
}
