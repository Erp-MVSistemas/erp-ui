import { Component, OnInit } from '@angular/core';
import { Empresa } from '../../../classes/empresa';
import { VariaveisGlobais } from '../../../classes/variaveis-globais';
import { ConfirmationService } from 'primeng/api';
import { EmpresaService } from '../../../servicos/empresa.service';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css'],
  styles: [`
  /* Column Priorities */
  @media only all {
      th.ui-p-6,
      td.ui-p-6,
      th.ui-p-5,
      td.ui-p-5,
      th.ui-p-4,
      td.ui-p-4,
      th.ui-p-3,
      td.ui-p-3,
      th.ui-p-2,
      td.ui-p-2,
      th.ui-p-1,
      td.ui-p-1 {
          display: none;
      }
  }

  /* Show priority 1 at 320px (20em x 16px) */
  @media screen and (min-width: 20em) {
      th.ui-p-1,
      td.ui-p-1 {
          display: table-cell;
      }
  }

  /* Show priority 2 at 480px (30em x 16px) */
  @media screen and (min-width: 30em) {
      th.ui-p-2,
      td.ui-p-2 {
          display: table-cell;
      }
  }

  /* Show priority 3 at 640px (40em x 16px) */
  @media screen and (min-width: 40em) {
      th.ui-p-3,
      td.ui-p-3 {
          display: table-cell;
      }
  }

  /* Show priority 4 at 800px (50em x 16px) */
  @media screen and (min-width: 50em) {
      th.ui-p-4,
      td.ui-p-4 {
          display: table-cell;
      }
  }

  /* Show priority 5 at 960px (60em x 16px) */
  @media screen and (min-width: 60em) {
      th.ui-p-5,
      td.ui-p-5 {
          display: table-cell;
      }
  }

  /* Show priority 6 at 1,120px (70em x 16px) */
  @media screen and (min-width: 70em) {
      th.ui-p-6,
      td.ui-p-6 {
          display: table-cell;
      }
  }
`]
})
export class EmpresaComponent implements OnInit {

  colunas: any[];
  empresas: Empresa[];
  empresaSelecionada: Empresa;

  constructor(private confirmationService: ConfirmationService,
    private global: VariaveisGlobais,
    private empresaService: EmpresaService) {
      this.global.tituloJanela = '';
      this.global.botaoDesabilitado = true;
     }

  ngOnInit() {
    this.global.tituloJanela = 'Cadastro de Empresas';
    this.empresaSelecionada = new Empresa();
  }

  consultar() {
    this.carregarDados();
  }

  getEditarEmpresa() {
    return this.empresaSelecionada.id == null ? null : this.empresaSelecionada.id;
  }

  getExcluirEmpresa() {
    this.confirmationService.confirm({
      message: 'Excluir o registro selecionado?',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.empresaService.getRemover(this.empresaSelecionada.id).subscribe(
          dados => {
            this.carregarDados();
            this.global.mostraMensagem(this.global.info, 'Confirmar', 'Empresa Excluido!');
            this.global.botaoDesabilitado = true;
          },
          error => {
            this.global.mostraMensagem(this.global.error, 'Atenção', this.global.tratarError(error));
          }
        );
      },
      reject: () => {
      }
    });
  }

  onRowSelect(event) {
    this.global.botaoDesabilitado = false;
  }

  onRowUnselect(event) {
    if (this.empresaSelecionada == null) {
      this.empresaSelecionada = new Empresa();
    }
    this.global.botaoDesabilitado = true;
  }

  carregarDados() {
    this.empresaService.getEmpresas().subscribe(
      dados => {
        this.empresas = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', this.global.tratarError(error));
      }
    );
  }

}
