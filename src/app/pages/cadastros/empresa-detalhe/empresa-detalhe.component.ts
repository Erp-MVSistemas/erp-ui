import { VariaveisGlobais } from './../../../classes/variaveis-globais';
import { Component, OnInit } from '@angular/core';
import { EmpresaService } from '../../../servicos/empresa.service';
import { ViaCepService } from '../../../servicos/via-cep.service';
import { Address } from '../../../classes/address';
import { Empresa } from '../../../classes/empresa';
import { Telefone } from '../../../classes/telefone';
import { ConfirmationService } from 'primeng/api';
import { Location } from '@angular/common';


import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-empresa-detalhe',
  templateUrl: './empresa-detalhe.component.html',
  styleUrls: ['./empresa-detalhe.component.css']
})
export class EmpresaDetalheComponent implements OnInit {

  private empresa: Empresa;
  private telefone: Telefone;
  private telefoneSelecionado: Telefone;
  private crt: any[];
  private tipoEmpresa: any[];
  private tipo: string;
  private regime: string;


  constructor(private empresaService: EmpresaService,
    private viacep: ViaCepService,
    private global: VariaveisGlobais,
    private locate: Location,
    private confirmationService: ConfirmationService) {
    this.empresa = new Empresa();
    this.telefone = new Telefone();
  }

  ngOnInit() {
    this.crt = [
      { value: 1, label: 'SIMPLES NACIONAL' },
      { value: 2, label: 'SIMPLES NACIONAL – EXCESSO DE SUBLIMITE DE RECEITA BRUTA' },
      { value: 3, label: 'REGIME NORMAL' }
    ];

    this.tipoEmpresa = [
      { value: 1, label: { string: 'MATRIZ', texto: 'Matriz' } },
      { value: 2, label: { string: 'FILIAL', texto: 'Filial' } }
    ];
  }

  salvarEmpresa(form: NgForm) {
    this.empresaService.getSalvar(this.empresa).subscribe(
      obj => {
        this.empresa = obj;
        this.global.mostraMensagem(this.global.info, 'Sucesso!',
          this.empresa == null ? 'Empresa salvo com Sucesso' : 'Empresa alterado com Sucesso');
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  cancelar() {
    this.locate.back();
  }

  consultaCep() {
    if ((this.empresa.endereco.cep === undefined) || (this.empresa.endereco.cep === '')) {
      this.global.mostraMensagem(this.global.warn, 'Atenção', 'Informe o Cep');
    } else {
      const cepSemFormato = this.empresa.endereco.cep.replace(/[^0-9]+/g, '');
      this.viacep.getBuscarCep(cepSemFormato).subscribe(
        address => {
          if (address.erro === true) {
            this.empresa.endereco = undefined;
            this.global.mostraMensagem(this.global.error, 'Atenção', 'Cep não encontrado. ');
          } else {
            this.preencherEndereco(address);
          }
        },
        error => {
          this.global.mostraMensagem(this.global.error, 'Error', 'Error: ' + this.global.tratarError(error));
          this.empresa.endereco = undefined;
        }
      );
    }
  }

  onRowSelectTelefone(event) {
    this.global.botaoDesabilitado = false;
  }

  onRowUnselectTelefone(event) {
    this.global.botaoDesabilitado = true;
  }

  preencherEndereco(endereco: Address) {
    this.empresa.endereco.bairro = endereco.bairro;
    this.empresa.endereco.cidade.nome = endereco.localidade;
    this.empresa.endereco.logradouro = endereco.logradouro;
    this.empresa.endereco.cidade.estado.uf = endereco.uf;
  }

  inserirTelefone() {
    if ((this.telefone.numero === undefined) || (this.telefone.numero === '')) {
      this.global.mostraMensagem(this.global.warn, 'Atenção', 'Informe o numero de Telefone!');
    } else {
      for (let i = 0; i < this.empresa.telefones.length; i++) {
        if (this.telefone.numero === this.empresa.telefones[i].numero) {
          this.global.mostraMensagem(this.global.warn, 'Atenção', 'Numero de Telefone já inserido!');
          return;
        }
      }
      this.empresa.telefones.push(this.telefone);
    }

    this.telefone = new Telefone();
  }

  getExcluirTelefone() {
    this.confirmationService.confirm({
      message: 'Excluir o registro selecionado?',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.empresa.telefones.splice(this.findSelectedTelefoneIndex(), 1);
        this.global.botaoDesabilitado = true;
      },
      reject: () => {
      }
    });
  }

  findSelectedTelefoneIndex(): number {
    return this.empresa.telefones.indexOf(this.telefoneSelecionado);
  }

  selecionarTipoEmpresa(event) {
    this.empresa.tipoEmpresa = event.value.toString();
  }

  selecionarTipoRegime(event) {
    this.empresa.crt = event.value.toString();
  }
}
