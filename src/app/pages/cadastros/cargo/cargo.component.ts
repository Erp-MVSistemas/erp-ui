import { Cargo } from './../../../classes/cargo';
import { VariaveisGlobais } from './../../../classes/variaveis-globais';
import { CargoService } from './../../../servicos/cargo.service';
import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-cargo',
  templateUrl: './cargo.component.html',
  styleUrls: ['./cargo.component.css']
})
export class CargoComponent implements OnInit {

  cargos: Cargo[];
  cargoSelecionado: Cargo;
  colunas: any[];

  constructor(private cargoService: CargoService,
    private confirmationService: ConfirmationService,
    private global: VariaveisGlobais) {
      this.global.botaoDesabilitado = true;
      this.global.tituloJanela = '';
     }

  ngOnInit() {
    this.global.tituloJanela = 'Cadastro de Cargos';
    this.cargoSelecionado = new Cargo();
  }

  consultar() {
    this.carregaDados();
  }

  getEditarCargo(obj: Cargo) {
    return obj.id;
  }

  getExcluirCargo(obj: Cargo) {
    this.confirmationService.confirm({
      message: 'Excluir o registro selecionado?',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.cargoService.getRemover(obj.id).subscribe(
          dados => {
            this.carregaDados();
            this.global.mostraMensagem(this.global.info, 'Confirmar', 'cargo Excluido!');
            this.global.botaoDesabilitado = true;
          },
          error => {
            this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
          }
        );
      },
      reject: () => {
      }
    });
  }

  onRowSelect(event) {
    this.global.botaoDesabilitado = false;
  }

  onRowUnselect(event) {
    if (this.cargoSelecionado == null) {
      this.cargoSelecionado = new Cargo();
    }
    this.global.botaoDesabilitado = true;
  }

  private carregaDados() {
    this.cargoService.getCargos().subscribe(
      dados => {
        this.cargos = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', this.global.tratarError(error));
      }
    );
  }

}
