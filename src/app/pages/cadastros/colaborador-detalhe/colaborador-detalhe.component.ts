import { Message } from 'primeng/api';
import { Cargo } from './../../../classes/cargo';
import { CargoService } from './../../../servicos/cargo.service';
import { VariaveisGlobais } from './../../../classes/variaveis-globais';
import { ColaboradorService } from './../../../servicos/colaborador.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Colaborador } from './../../../classes/colaborador';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Pessoa } from '../../../classes/pessoa';
import { PessoaService } from '../../../servicos/pessoa.service';
import { SetorService } from '../../../servicos/setor.service';
import { TipoColaboradorService } from '../../../servicos/tipo-colaborador.service';
import { Setor } from '../../../classes/setor';
import { TipoColaborador } from '../../../classes/tipo-colaborador';
import { environment } from '../../../../environments/environment';
import { Usuario } from '../../../classes/usuario';

@Component({
  selector: 'app-colaborador-detalhe',
  templateUrl: './colaborador-detalhe.component.html',
  styleUrls: ['./colaborador-detalhe.component.css']
})
export class ColaboradorDetalheComponent implements OnInit {

  colaborador: Colaborador;
  colaboradorSelecionado: Colaborador;
  filtroPessoa: Pessoa[];
  filtroSetor: Setor[];
  filtroCargo: Cargo[];
  filtroTipoColaborador: TipoColaborador[];
  urlUpload: string;
  urlFoto: any;
  imagem: Blob;
  uploadedFiles: any[] = [];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private locate: Location,
    private colaboradorService: ColaboradorService,
    private setorService: SetorService,
    private tipoColaboradorService: TipoColaboradorService,
    private cargoService: CargoService,
    private pessoaService: PessoaService,
    private global: VariaveisGlobais) {
    this.colaborador = new Colaborador();
  }

  ngOnInit() {
    this.global.tituloJanela = null;
    this.global.tituloJanela = 'Adicionar Colaborador';
    this.urlUpload = environment.enderecoUrl + 'colaborador/upload/';
    this.colaborador = new Colaborador();

    if (this.router.url !== '/colaborador/novo') {
      this.global.tituloJanela = null;
      this.global.tituloJanela = 'Editando Colaborador';
      const id = this.route.snapshot.paramMap.get('id');
      this.colaboradorService.getColaborador(parseInt(id, 0)).subscribe(
        dados => {
          this.colaborador = dados;
          this.urlFoto = this.colaborador.foto34;
        },
        error => {
          this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
        }
      );
    }
  }

  cancelar() {
    this.locate.back();
  }

  salvar() {
    this.colaboradorService.getSalvar(this.colaborador).subscribe(
      dados => {
        this.colaborador = dados;
        this.global.mostraMensagem(this.global.info, 'Sucesso!',
          this.colaborador == null ? 'Colaborador salvo com Sucesso' : 'Colaborador alterado com Sucesso');
        this.cancelar();
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
    this.colaborador = new Colaborador();
  }

  getEditarColaborador() {
    return this.colaboradorSelecionado == null ? null : this.colaboradorSelecionado.id;
  }

  buscaPessoa(event) {
    this.pessoaService.getListaPessoas(event.query).subscribe(
      dados => {
        this.filtroPessoa = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaSetor(event) {
    this.setorService.getListaSetores(event.query).subscribe(
      dados => {
        this.filtroSetor = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaCargo(event) {
    this.cargoService.getListaCargos(event.query).subscribe(
      dados => {
        this.filtroCargo = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaTipoColaborador(event) {
    this.tipoColaboradorService.getListaTipoColaboradores(event.query).subscribe(
      dados => {
        this.filtroTipoColaborador = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  erroUpload(event) {
    const menssagem = JSON.parse(event.xhr.response);
    this.global.mostraMensagem(this.global.error, 'Atenção', menssagem.status + ' - ' + menssagem.message);
  }

  onBeforeSend(event) {
    const usuario: Usuario = JSON.parse(localStorage.getItem('usuarioSessao'));
    event.xhr.setRequestHeader('Authorization', usuario.token);
  }

  onUpload(event) {
    this.global.mostraMensagem(this.global.info, 'Sucesso', 'Upload realizado com Sucesso!');
  }

  atualizaUrlFoto() {
    this.urlFoto = environment.enderecoUrl.substring(0, environment.enderecoUrl.length - 1)
       + this.colaborador.foto34 + '?t-' + new Date().getTime();
     console.log('Atualizar foto: ' + this.urlFoto);
  }

  myUploader(event) {
    const arquivo = <File>event.files[0];
    this.uploadedFiles.push(event.files);
    const reader = new FileReader();
    reader.onloadend = (e) => {
      this.urlFoto = reader.result;
    };
    reader.readAsDataURL(arquivo);
    this.colaborador.foto34 = this.urlFoto;
  }
}
