import { CargoService } from './../../../servicos/cargo.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { VariaveisGlobais } from '../../../classes/variaveis-globais';
import { Cargo } from '../../../classes/cargo';

@Component({
  selector: 'app-cargo-detalhe',
  templateUrl: './cargo-detalhe.component.html',
  styleUrls: ['./cargo-detalhe.component.css']
})
export class CargoDetalheComponent implements OnInit {

  cargo: Cargo;
  cargoSelecionado: Cargo;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private locate: Location,
    private cargoService: CargoService,
    private global: VariaveisGlobais) {
      this.global.tituloJanela = '';
      this.cargo = new Cargo();
    }

  ngOnInit() {
    this.global.tituloJanela = 'Adiconar cargo';
    this.cargo = new Cargo();
    if (this.router.url !== '/cargo/novo') {
      const id = this.route.snapshot.paramMap.get('id');
      this.cargoService.getCargo(parseInt(id, 0)).subscribe(
        dados => {
          this.cargo = dados;
        },
        error => {
          this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
        }
      );
      this.global.tituloJanela = 'Editando cargo';
    }
  }

  cancelar() {
    this.locate.back();
  }

  salvar() {
    this.cargoService.getSalvar(this.cargo).subscribe(
      dados => {
        this.cargo = dados;
        this.global.mostraMensagem(this.global.info, 'Sucesso!',
          this.cargo == null ? 'Cargo salvo com Sucesso' : 'Cargo alterado com Sucesso');
          this.cancelar();
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
    this.cargo = new Cargo();
  }

  getEditarCargo() {
    return this.cargoSelecionado == null ? null : this.cargoSelecionado.id;
  }
}
