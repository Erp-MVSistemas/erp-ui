import { VariaveisGlobais } from './../../../classes/variaveis-globais';
import { ConfirmationService } from 'primeng/api';
import { ColaboradorService } from './../../../servicos/colaborador.service';
import { Colaborador } from './../../../classes/colaborador';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-colaborador',
  templateUrl: './colaborador.component.html',
  styleUrls: ['./colaborador.component.css'],
  styles: [`
  /* Column Priorities */
  @media only all {
      th.ui-p-6,
      td.ui-p-6,
      th.ui-p-5,
      td.ui-p-5,
      th.ui-p-4,
      td.ui-p-4,
      th.ui-p-3,
      td.ui-p-3,
      th.ui-p-2,
      td.ui-p-2,
      th.ui-p-1,
      td.ui-p-1 {
          display: none;
      }
  }

  /* Show priority 1 at 320px (20em x 16px) */
  @media screen and (min-width: 20em) {
      th.ui-p-1,
      td.ui-p-1 {
          display: table-cell;
      }
  }

  /* Show priority 2 at 480px (30em x 16px) */
  @media screen and (min-width: 30em) {
      th.ui-p-2,
      td.ui-p-2 {
          display: table-cell;
      }
  }

  /* Show priority 3 at 640px (40em x 16px) */
  @media screen and (min-width: 40em) {
      th.ui-p-3,
      td.ui-p-3 {
          display: table-cell;
      }
  }

  /* Show priority 4 at 800px (50em x 16px) */
  @media screen and (min-width: 50em) {
      th.ui-p-4,
      td.ui-p-4 {
          display: table-cell;
      }
  }

  /* Show priority 5 at 960px (60em x 16px) */
  @media screen and (min-width: 60em) {
      th.ui-p-5,
      td.ui-p-5 {
          display: table-cell;
      }
  }

  /* Show priority 6 at 1,120px (70em x 16px) */
  @media screen and (min-width: 70em) {
      th.ui-p-6,
      td.ui-p-6 {
          display: table-cell;
      }
  }
`]
})
export class ColaboradorComponent implements OnInit {

  colaboradores: Colaborador[];
  colaboradorSelecionado: Colaborador;
  colunas: any[];

  constructor(private colaboradorService: ColaboradorService,
    private confirmationService: ConfirmationService,
    private global: VariaveisGlobais) {
      this.global.tituloJanela = '';
      this.global.botaoDesabilitado = true;
    }

  ngOnInit() {
    this.global.tituloJanela = 'Cadastro de Colaboradores';
    this.colaboradorSelecionado = new Colaborador();
  }

  consultar() {
    this.carregaDados();
  }

  getEditarColaborador(obj: Colaborador) {
    return obj.id;
  }

  getExcluirColaborador(obj: Colaborador) {
    this.confirmationService.confirm({
      message: 'Excluir o registro selecionado?',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.colaboradorService.getRemover(obj.id).subscribe(
          dados => {
            this.carregaDados();
            this.global.mostraMensagem(this.global.info, 'Confirmar', 'colaborador Excluido!');
            this.global.botaoDesabilitado = true;
          },
          error => {
            this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
          }
        );
      },
      reject: () => {
      }
    });
  }

  private carregaDados() {
    this.colaboradorService.getColaboradores().subscribe(
      dados => {
        this.colaboradores = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', this.global.tratarError(error));
      }
    );
  }

}
