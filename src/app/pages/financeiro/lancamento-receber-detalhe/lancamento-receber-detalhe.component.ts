import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FinLancamentoReceber } from 'src/app/classes/fin-lancamento-receber';
import { Cliente } from 'src/app/classes/cliente';
import { FinNaturezaFinanceira } from 'src/app/classes/fin-natureza-financeira';
import { FinDocumentoOrigem } from 'src/app/classes/fin-documento-origem';
import { ContaCaixa } from 'src/app/classes/conta-caixa';
import { SelectItem } from 'primeng/api';
import { Location } from '@angular/common';
import { LancamentoReceberService } from 'src/app/servicos/lancamento-receber.service';
import { NaturezaFinanceiraService } from 'src/app/servicos/natureza-financeira.service';
import { DocumentoOrigemService } from 'src/app/servicos/documento-origem.service';
import { ClienteService } from 'src/app/servicos/cliente.service';
import { ContaCaixaService } from 'src/app/servicos/conta-caixa.service';
import { VariaveisGlobais } from 'src/app/classes/variaveis-globais';

@Component({
  selector: 'app-lancamento-receber-detalhe',
  templateUrl: './lancamento-receber-detalhe.component.html',
  styleUrls: ['./lancamento-receber-detalhe.component.css']
})
export class LancamentoReceberDetalheComponent implements OnInit {

  lancamentoReceber: FinLancamentoReceber;
  filtroCliente: Cliente[];
  filtroNaturezaFinanceira: FinNaturezaFinanceira[];
  filtroDocumentoOrigem: FinDocumentoOrigem[];
  filtroContaCaixa: ContaCaixa[];
  ptBR: any;
  display: boolean;
  opcoesTipoReajuste: SelectItem[];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private locate: Location,
    private lancamentoReceberService: LancamentoReceberService,
    private natuerezaFinanceiraService: NaturezaFinanceiraService,
    private documentoOrigemService: DocumentoOrigemService,
    private clienteService: ClienteService,
    private contaCaixaService: ContaCaixaService,
    private global: VariaveisGlobais) {
    this.display = false;
  }

  ngOnInit() {
    this.global.tituloJanela = null;
    this.global.tituloJanela = 'Detalhe Lançamento a Receber';
    this.lancamentoReceber = new FinLancamentoReceber();
    this.global.botaoDesabilitado = true;

    this.ptBR = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
        'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };

    if (this.router.url !== '/lancamento-receber/novo') {
      this.global.tituloJanela = null;
      this.global.tituloJanela = 'Editando Lançamento a Receber';
      const id = this.route.snapshot.paramMap.get('id');
      this.lancamentoReceberService.getFinLancamentoReceber(parseInt(id, 0)).subscribe(
        dados => {
          this.lancamentoReceber = dados;
        },
        error => {
          this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
        }
      );
    }
  }

  cancelar() {
    this.locate.back();
  }

  salvar() {

    // if (this.lancamentoReceber.listaFinParcelaReceber === undefined ||
    //   this.lancamentoReceber.listaFinParcelaReceber === []) {
    //     this.global.mostraMensagem(this.global.warn, 'Atenção', 'Inserir pelo menos 1 item para reajuste!');
    //     return;
    // }

    this.lancamentoReceberService.getSalvar(this.lancamentoReceber).subscribe(
      dados => {
        this.buscarLancamento(dados);
        this.global.mostraMensagem(this.global.info, 'Sucesso!', 'Reajuste salvo com Sucesso');
        // this.cancelar();
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
    this.lancamentoReceber = new FinLancamentoReceber();
  }

  buscarLancamento(obj: FinLancamentoReceber) {
    return this.lancamentoReceberService.getFinLancamentoReceber(obj.id).subscribe(
      dados => {
        this.lancamentoReceber = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaCliente(event) {
    this.clienteService.getListaClientes(event.query).subscribe(
      dados => {
        this.filtroCliente = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaNaturezaFinanceira(event) {
    this.natuerezaFinanceiraService.getListaFinNaturezaFinanceira(event.query).subscribe(
      dados => {
        this.filtroNaturezaFinanceira = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaDocumentoOrigem(event) {
    this.documentoOrigemService.getListaFinDocumentoOrigem(event.query).subscribe(
      dados => {
        this.filtroDocumentoOrigem = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaContaCaixa(event) {
    this.contaCaixaService.getListaContaCaixa(event.query).subscribe(
      dados => {
        this.filtroContaCaixa  = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  cancelarInclusao() {
    this.lancamentoReceber = new FinLancamentoReceber();
    this.display = false;
  }

}
