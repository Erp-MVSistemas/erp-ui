import { ParcelaPagamentoService } from './../../../servicos/parcela-pagamento.service';
import { FinParcelaPagamento } from './../../../classes/fin-parcela-pagamento';
import { VariaveisGlobais } from './../../../classes/variaveis-globais';
import { ConfirmationService } from 'primeng/api';
import { Component, OnInit } from '@angular/core';
import { FinParcelaPagar } from 'src/app/classes/fin-parcela-pagar';
import { ObjectUtils } from 'primeng/components/utils/objectutils';

@Component({
  selector: 'app-parcela-pagamento',
  templateUrl: './parcela-pagamento.component.html',
  styleUrls: ['./parcela-pagamento.component.css']
})
export class ParcelaPagamentoComponent implements OnInit {

  listaParcelaPagar: FinParcelaPagar[];
  cols: any[];
  parcelaPagarSelecionado: FinParcelaPagar;
  ptBR: any;

  constructor(private parcelaPagamentoService: ParcelaPagamentoService,
    private confirmationService: ConfirmationService,
    private global: VariaveisGlobais) {
      this.global.botaoDesabilitado = true;
    }

  ngOnInit() {
    this.global.tituloJanela = 'Pagamento de Parcela';
    this.ptBR = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
                    'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };
  }

  consultarReajuste() {
    this.carregaDados();
  }

  private carregaDados() {
    this.parcelaPagamentoService.getListaFinParcelaPagar().subscribe(
      lista => {
        this.listaParcelaPagar = lista;
        console.log(lista);
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Ocorreu um erro', this.global.tratarError(error));
      }
    );
  }

  getEditar(obj: FinParcelaPagamento) {
    return obj.id;
  }
}
