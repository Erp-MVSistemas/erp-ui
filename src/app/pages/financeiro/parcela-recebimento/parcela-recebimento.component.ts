import { Component, OnInit } from '@angular/core';
import { FinParcelaReceber } from 'src/app/classes/fin-parcela-receber';
import { ParcelaRecebimentoService } from 'src/app/servicos/parcela-recebimento.service';
import { ConfirmationService } from 'primeng/api';
import { VariaveisGlobais } from 'src/app/classes/variaveis-globais';
import { FinParcelaRecebimento } from 'src/app/classes/fin-parcela-recebimento';

@Component({
  selector: 'app-parcela-recebimento',
  templateUrl: './parcela-recebimento.component.html',
  styleUrls: ['./parcela-recebimento.component.css']
})
export class ParcelaRecebimentoComponent implements OnInit {

  listaParcelaReceber: FinParcelaReceber[];
  cols: any[];
  parcelaReceberSelecionado: FinParcelaReceber;
  ptBR: any;

  constructor(private parcelaRecebimentoService: ParcelaRecebimentoService,
    private confirmationService: ConfirmationService,
    private global: VariaveisGlobais) {
      this.global.botaoDesabilitado = true;
    }

  ngOnInit() {
    this.global.tituloJanela = 'Recebimento de Parcela';
    this.ptBR = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
                    'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };
  }

  consultarReajuste() {
    this.carregaDados();
  }

  private carregaDados() {
    this.parcelaRecebimentoService.getListaFinParcelaReceber().subscribe(
      lista => {
        this.listaParcelaReceber = lista;
        console.log(lista);
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Ocorreu um erro', this.global.tratarError(error));
      }
    );
  }

  getEditar(obj: FinParcelaRecebimento) {
    return obj.id;
  }

}
