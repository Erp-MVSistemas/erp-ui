import { ConfirmationService } from 'primeng/api';
import { Component, OnInit } from '@angular/core';
import { FinLancamentoReceber } from 'src/app/classes/fin-lancamento-receber';
import { VariaveisGlobais } from 'src/app/classes/variaveis-globais';
import { LancamentoReceberService } from 'src/app/servicos/lancamento-receber.service';

@Component({
  selector: 'app-lancamento-receber',
  templateUrl: './lancamento-receber.component.html',
  styleUrls: ['./lancamento-receber.component.css'],
  styles: [`
  /* Column Priorities */
  @media only all {
      th.ui-p-6,
      td.ui-p-6,
      th.ui-p-5,
      td.ui-p-5,
      th.ui-p-4,
      td.ui-p-4,
      th.ui-p-3,
      td.ui-p-3,
      th.ui-p-2,
      td.ui-p-2,
      th.ui-p-1,
      td.ui-p-1 {
          display: none;
      }
  }

  /* Show priority 1 at 320px (20em x 16px) */
  @media screen and (min-width: 20em) {
      th.ui-p-1,
      td.ui-p-1 {
          display: table-cell;
      }
  }

  /* Show priority 2 at 480px (30em x 16px) */
  @media screen and (min-width: 30em) {
      th.ui-p-2,
      td.ui-p-2 {
          display: table-cell;
      }
  }

  /* Show priority 3 at 640px (40em x 16px) */
  @media screen and (min-width: 40em) {
      th.ui-p-3,
      td.ui-p-3 {
          display: table-cell;
      }
  }

  /* Show priority 4 at 800px (50em x 16px) */
  @media screen and (min-width: 50em) {
      th.ui-p-4,
      td.ui-p-4 {
          display: table-cell;
      }
  }

  /* Show priority 5 at 960px (60em x 16px) */
  @media screen and (min-width: 60em) {
      th.ui-p-5,
      td.ui-p-5 {
          display: table-cell;
      }
  }

  /* Show priority 6 at 1,120px (70em x 16px) */
  @media screen and (min-width: 70em) {
      th.ui-p-6,
      td.ui-p-6 {
          display: table-cell;
      }
  }
`]
})
export class LancamentoReceberComponent implements OnInit {

  listaLancamentoReceber: FinLancamentoReceber[];
  cols: any[];
  lancamentoReceberSelecionado: FinLancamentoReceber;
  ptBR: any;

  constructor(private lancamentoReceberService: LancamentoReceberService,
    private confirmationService: ConfirmationService,
    private global: VariaveisGlobais) {
      this.global.botaoDesabilitado = true;
    }

  ngOnInit() {
    this.global.tituloJanela = 'Lançamentos a Receber';
    this.ptBR = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
                    'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };
  }

  consultarReajuste() {
    this.carregaDados();
  }

  private carregaDados() {
    this.lancamentoReceberService.getListaFinLancamentoReceber().subscribe(
      lista => {
        this.listaLancamentoReceber = lista;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Ocorreu um erro', this.global.tratarError(error));
      }
    );
  }

  getEditar(obj: FinLancamentoReceber) {
    return obj.id;
  }

  getExcluir(obj: FinLancamentoReceber) {
    this.confirmationService.confirm({
      message: 'Excluir o registro selecionado?',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.lancamentoReceberService.getExcluir(obj.id).subscribe(
          _ => {
            this.carregaDados();
            this.global.mostraMensagem(this.global.info, 'Confirmação', 'Registro excluído com sucesso');
            this.global.botaoDesabilitado = true;
          },
          error => {
            this.global.mostraMensagem(this.global.error, 'Ocorreu um erro', this.global.tratarError(error));
          }
        );
      },
      reject: () => {
        // this.msgs = [{ severity: 'info', summary: 'Rejected', detail: 'You have rejected' }];
      }
    });
  }

}
