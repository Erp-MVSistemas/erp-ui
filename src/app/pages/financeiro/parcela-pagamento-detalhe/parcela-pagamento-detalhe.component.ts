import { FinTipoPagamento } from './../../../classes/fin-tipo-pagamento';
import { TipoPagamentoService } from './../../../servicos/tipo-pagamento.service';
import { Component, OnInit } from '@angular/core';
import { FinParcelaPagar } from 'src/app/classes/fin-parcela-pagar';
import { ParcelaPagamentoService } from 'src/app/servicos/parcela-pagamento.service';
import { ActivatedRoute, Router } from '@angular/router';
import { VariaveisGlobais } from 'src/app/classes/variaveis-globais';
import { ContaCaixa } from 'src/app/classes/conta-caixa';
import { FinStatusParcela } from 'src/app/classes/fin-status-parcela';
import { Location } from '@angular/common';
import { FinParcelaPagamento } from 'src/app/classes/fin-parcela-pagamento';
import * as moment from 'moment';

@Component({
  selector: 'app-parcela-pagamento-detalhe',
  templateUrl: './parcela-pagamento-detalhe.component.html',
  styleUrls: ['./parcela-pagamento-detalhe.component.css']
})
export class ParcelaPagamentoDetalheComponent implements OnInit {

  parcelaPagar: FinParcelaPagar;
  listaParcelaPagamento: FinParcelaPagamento[];
  parcelaPagamento: FinParcelaPagamento;
  opcoesTipoPagamento: FinTipoPagamento[];
  display: Boolean = false;
  valorPagar: number;

  constructor(private parcelaPagamentoService: ParcelaPagamentoService,
    private tipoPagamentoService: TipoPagamentoService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router,
    private global: VariaveisGlobais) { }

  ngOnInit() {
    this.display = false;
    this.global.tituloJanela = 'Pagamento de Parcela';
    this.parcelaPagar = new FinParcelaPagar();
    this.parcelaPagar.contaCaixa = new ContaCaixa();
    this.parcelaPagar.finStatusParcela = new FinStatusParcela();
    this.parcelaPagamento = new FinParcelaPagamento();
    this.parcelaPagamento.finTipoPagamento = new FinTipoPagamento();

    const id = this.route.snapshot.paramMap.get('id');
    this.parcelaPagamentoService.getFinParcelaPagar(parseInt(id, 0)).subscribe(
      obj => {
        this.parcelaPagar = obj;
        this.valorPagar = this.parcelaPagar.valor - this.valorParcelasPagas();
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Ocorreu um erro', this.global.tratarError(error));
      }
    );
  }

  retornar() {
    this.location.back();
  }

  salvar() {
    this.parcelaPagamentoService.getSalvar(this.parcelaPagar).subscribe(
      obj => {
        this.parcelaPagar = obj;
        this.global.mostraMensagem(this.global.info, 'Confirmação', 'Registro salvo com sucesso');
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Ocorreu um erro', this.global.tratarError(error));
      }
    );
  }

  pagamento() {
    this.tiposPagamento();
    this.display = true;
  }

  cancelar() {
    this.location.back();
  }

  cancelarPagamento() {
    this.display = false;
  }

  incluirPagamento() {
    const agora = moment();
    if (this.parcelaPagar.listaFinParcelaPagamento === undefined) {
      this.parcelaPagar.listaFinParcelaPagamento = [];
    }
    this.parcelaPagamento.finParcelaPagar = this.parcelaPagar;
    this.parcelaPagamento.contaCaixa = this.parcelaPagar.contaCaixa;
    this.parcelaPagamento.dataPagamento = agora.format('YYYY-MM-DD');
    this.parcelaPagar.listaFinParcelaPagamento.push(this.parcelaPagamento);
    this.valorPagar = this.parcelaPagar.valor - this.valorParcelasPagas();
    this.display = false;
  }

  valorParcelasPagas() {
    let valorBaixado = 0;
    this.parcelaPagar.listaFinParcelaPagamento.forEach(
      valor => {
        valorBaixado = valor.valorPago;
      }
    );
    return valorBaixado;
  }

  tiposPagamento() {
    if (this.opcoesTipoPagamento === undefined) {
      this.opcoesTipoPagamento = [];
    }
    this.tipoPagamentoService.getListaFinTipoPagamento().subscribe(
      tipos => {
        this.opcoesTipoPagamento = tipos;
      }
    );
  }

}
