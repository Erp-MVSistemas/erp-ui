import { Component, OnInit } from '@angular/core';
import { FinParcelaReceber } from 'src/app/classes/fin-parcela-receber';
import { FinParcelaRecebimento } from 'src/app/classes/fin-parcela-recebimento';
import { FinTipoRecebimento } from 'src/app/classes/fin-tipo-recebimento';
import { ParcelaRecebimentoService } from 'src/app/servicos/parcela-recebimento.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { VariaveisGlobais } from 'src/app/classes/variaveis-globais';
import { TipoRecebimentoService } from 'src/app/servicos/tipo-recebimento.service';
import { ContaCaixa } from 'src/app/classes/conta-caixa';
import { FinStatusParcela } from 'src/app/classes/fin-status-parcela';
import * as moment from 'moment';

@Component({
  selector: 'app-parcela-recebimento-detalhe',
  templateUrl: './parcela-recebimento-detalhe.component.html',
  styleUrls: ['./parcela-recebimento-detalhe.component.css']
})
export class ParcelaRecebimentoDetalheComponent implements OnInit {

  parcelaReceber: FinParcelaReceber;
  listaParcelaRecebimento: FinParcelaRecebimento[];
  parcelaRecebimento: FinParcelaRecebimento;
  opcoesTipoRecebimento: FinTipoRecebimento[];
  display: Boolean = false;
  valorReceber: number;

  constructor(private parcelaRecebimentoService: ParcelaRecebimentoService,
    private tipoRecebimentoService: TipoRecebimentoService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router,
    private global: VariaveisGlobais) { }

  ngOnInit() {
    this.display = false;
    this.global.tituloJanela = 'Recebimento de Parcela';
    this.parcelaReceber = new FinParcelaReceber();
    this.parcelaReceber.contaCaixa = new ContaCaixa();
    this.parcelaReceber.finStatusParcela = new FinStatusParcela();
    this.parcelaRecebimento = new FinParcelaRecebimento();
    this.parcelaRecebimento.finTipoRecebimento = new FinTipoRecebimento();

    const id = this.route.snapshot.paramMap.get('id');
    this.parcelaRecebimentoService.getFinParcelaReceber(parseInt(id, 0)).subscribe(
      obj => {
        this.parcelaReceber = obj;
        this.valorReceber = this.parcelaReceber.valor - this.valorParcelasPagas();
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Ocorreu um erro', this.global.tratarError(error));
      }
    );
  }

  retornar() {
    this.location.back();
  }

  salvar() {
    this.parcelaRecebimentoService.getSalvar(this.parcelaReceber).subscribe(
      obj => {
        this.parcelaReceber = obj;
        this.global.mostraMensagem(this.global.info, 'Confirmação', 'Registro salvo com sucesso');
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Ocorreu um erro', this.global.tratarError(error));
      }
    );
  }

  recebimento() {
    this.tiposRecebimento();
    this.display = true;
  }

  cancelar() {
    this.location.back();
  }

  cancelarRecebimento() {
    this.display = false;
  }

  incluirRecebimento() {
    const agora = moment();
    if (this.parcelaReceber.listaFinParcelaRecebimento === undefined) {
      this.parcelaReceber.listaFinParcelaRecebimento = [];
    }
    this.parcelaRecebimento.finParcelaReceber = this.parcelaReceber;
    this.parcelaRecebimento.contaCaixa = this.parcelaReceber.contaCaixa;
    this.parcelaRecebimento.dataRecebimento = agora.format('YYYY-MM-DD');
    this.parcelaReceber.listaFinParcelaRecebimento.push(this.parcelaRecebimento);
    this.valorReceber = this.parcelaReceber.valor - this.valorParcelasPagas();
    this.display = false;
  }

  valorParcelasPagas() {
    let valorBaixado = 0;
    this.parcelaReceber.listaFinParcelaRecebimento.forEach(
      valor => {
        valorBaixado = valor.valorRecebido;
      }
    );
    return valorBaixado;
  }

  tiposRecebimento() {
    if (this.opcoesTipoRecebimento === undefined) {
      this.opcoesTipoRecebimento = [];
    }
    this.tipoRecebimentoService.getListaFinTipoRecebimento().subscribe(
      tipos => {
        this.opcoesTipoRecebimento = tipos;
      }
    );
  }

}
