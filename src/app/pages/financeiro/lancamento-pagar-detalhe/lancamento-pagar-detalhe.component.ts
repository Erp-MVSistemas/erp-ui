import { ContaCaixaService } from 'src/app/servicos/conta-caixa.service';
import { ContaCaixa } from './../../../classes/conta-caixa';
import { Component, OnInit } from '@angular/core';
import { FinLancamentoPagar } from 'src/app/classes/fin-lancamento-pagar';
import { FinNaturezaFinanceira } from 'src/app/classes/fin-natureza-financeira';
import { FinDocumentoOrigem } from 'src/app/classes/fin-documento-origem';
import { Fornecedor } from 'src/app/classes/fornecedor';
import { LancamentoPagarService } from 'src/app/servicos/lancamento-pagar.service';
import { NaturezaFinanceiraService } from 'src/app/servicos/natureza-financeira.service';
import { DocumentoOrigemService } from 'src/app/servicos/documento-origem.service';
import { FornecedorService } from 'src/app/servicos/fornecedor.service';
import { VariaveisGlobais } from 'src/app/classes/variaveis-globais';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-lancamento-pagar-detalhe',
  templateUrl: './lancamento-pagar-detalhe.component.html',
  styleUrls: ['./lancamento-pagar-detalhe.component.css']
})
export class LancamentoPagarDetalheComponent implements OnInit {

  lancamentoPagar: FinLancamentoPagar;
  filtroFornecedor: Fornecedor[];
  filtroNaturezaFinanceira: FinNaturezaFinanceira[];
  filtroDocumentoOrigem: FinDocumentoOrigem[];
  filtroContaCaixa: ContaCaixa[];
  ptBR: any;
  display: boolean;
  opcoesTipoReajuste: SelectItem[];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private locate: Location,
    private lancamentoPagarService: LancamentoPagarService,
    private natuerezaFinanceiraService: NaturezaFinanceiraService,
    private documentoOrigemService: DocumentoOrigemService,
    private fornecedorService: FornecedorService,
    private contaCaixaService: ContaCaixaService,
    private global: VariaveisGlobais) {
    this.display = false;
  }

  ngOnInit() {
    this.global.tituloJanela = null;
    this.global.tituloJanela = 'Detalhe Lançamento a Pagar';
    this.lancamentoPagar = new FinLancamentoPagar();
    this.global.botaoDesabilitado = true;

    this.ptBR = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
        'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };

    if (this.router.url !== '/lancamento-pagar/novo') {
      this.global.tituloJanela = null;
      this.global.tituloJanela = 'Editando Lançamento a Pagar';
      const id = this.route.snapshot.paramMap.get('id');
      this.lancamentoPagarService.getFinLancamentoPagar(parseInt(id, 0)).subscribe(
        dados => {
          this.lancamentoPagar = dados;
        },
        error => {
          this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
        }
      );
    }
  }

  cancelar() {
    this.locate.back();
  }

  salvar() {

    // if (this.lancamentoPagar.listaFinParcelaPagar === undefined ||
    //   this.lancamentoPagar.listaFinParcelaPagar === []) {
    //     this.global.mostraMensagem(this.global.warn, 'Atenção', 'Inserir pelo menos 1 item para reajuste!');
    //     return;
    // }

    this.lancamentoPagarService.getSalvar(this.lancamentoPagar).subscribe(
      dados => {
        this.buscarLancamento(dados);
        this.global.mostraMensagem(this.global.info, 'Sucesso!', 'Reajuste salvo com Sucesso');
        // this.cancelar();
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
    this.lancamentoPagar = new FinLancamentoPagar();
  }

  buscarLancamento(obj: FinLancamentoPagar) {
    return this.lancamentoPagarService.getFinLancamentoPagar(obj.id).subscribe(
      dados => {
        this.lancamentoPagar = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaFornecedor(event) {
    this.fornecedorService.getListaFornecedores(event.query).subscribe(
      dados => {
        this.filtroFornecedor = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaNaturezaFinanceira(event) {
    this.natuerezaFinanceiraService.getListaFinNaturezaFinanceira(event.query).subscribe(
      dados => {
        this.filtroNaturezaFinanceira = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaDocumentoOrigem(event) {
    this.documentoOrigemService.getListaFinDocumentoOrigem(event.query).subscribe(
      dados => {
        this.filtroDocumentoOrigem = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaContaCaixa(event) {
    this.contaCaixaService.getListaContaCaixa(event.query).subscribe(
      dados => {
        this.filtroContaCaixa  = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  cancelarInclusao() {
    this.lancamentoPagar = new FinLancamentoPagar();
    this.display = false;
  }

}
