import { SelectItem } from 'primeng/api';
import { Component, OnInit } from '@angular/core';
import { EstoqueContagemCabecalho } from 'src/app/classes/estoque-contagem-cabecalho';
import { Produto } from 'src/app/classes/produto';
import { EstoqueContagemService } from 'src/app/servicos/estoque-contagem.service';
import { ProdutoService } from 'src/app/servicos/produto.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { VariaveisGlobais } from 'src/app/classes/variaveis-globais';
import { EstoqueContagemDetalhe } from 'src/app/classes/estoque-contagem-detalhe';

@Component({
  selector: 'app-contagem-detalhe',
  templateUrl: './contagem-detalhe.component.html',
  styleUrls: ['./contagem-detalhe.component.css']
})
export class ContagemDetalheComponent implements OnInit {

  estoqueContagem: EstoqueContagemCabecalho;
  filtroProduto: Produto[];
  produto: Produto;
  ptBR: any;
  display: boolean;
  opcoesEstoqueAtalizado: SelectItem[];
  quantidade: number;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private locate: Location,
    private produtoService: ProdutoService,
    private estoqueContagemService: EstoqueContagemService,
    private global: VariaveisGlobais) {
    this.display = false;
  }

  ngOnInit() {
    this.global.tituloJanela = null;
    this.global.tituloJanela = 'Detalhe Contagem';
    this.estoqueContagem = new EstoqueContagemCabecalho();
    this.global.botaoDesabilitado = true;

    this.opcoesEstoqueAtalizado = [
      {label: 'Sim', value: 'S'},
      {label: 'Não', value: 'N'}
    ];

    this.ptBR = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
        'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };

    if (this.router.url !== '/estoque-contagem/novo') {
      this.global.tituloJanela = null;
      this.global.tituloJanela = 'Editando Contagem';
      const id = this.route.snapshot.paramMap.get('id');
      this.estoqueContagemService.getEstoqueContagemCabecalho(parseInt(id, 0)).subscribe(
        dados => {
          this.estoqueContagem = dados;
        },
        error => {
          this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
        }
      );
    }
  }

  cancelar() {
    this.locate.back();
  }

  salvar() {

    if (this.estoqueContagem.listaEstoqueContagemDetalhe === undefined ||
      this.estoqueContagem.listaEstoqueContagemDetalhe === []) {
        this.global.mostraMensagem(this.global.warn, 'Atenção', 'Inserir pelo menos 1 item para Contagem!');
        return;
    }

    this.estoqueContagemService.getSalvar(this.estoqueContagem).subscribe(
      dados => {
        this.estoqueContagem = dados;
        this.global.mostraMensagem(this.global.info, 'Sucesso!', 'Contagem salvo com Sucesso');
        this.cancelar();
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
    this.estoqueContagem = new EstoqueContagemCabecalho();
  }

  buscaProduto(event) {
    this.produtoService.getListaProdutos(event.query).subscribe(
      dados => {
        this.filtroProduto = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  inserir() {
    this.display = true;
  }

  incluirProduto() {
    if (this.estoqueContagem.listaEstoqueContagemDetalhe === undefined) {
      this.estoqueContagem.listaEstoqueContagemDetalhe = [];
    }
    const detalhe: EstoqueContagemDetalhe = new EstoqueContagemDetalhe();
    detalhe.produto = this.produto;
    detalhe.quantidadeContada = this.quantidade;
    this.estoqueContagem.listaEstoqueContagemDetalhe.push(detalhe);

    this.display = false;
  }

  cancelarInclusao() {
    this.estoqueContagem = new EstoqueContagemCabecalho();
    this.display = false;
  }

}
