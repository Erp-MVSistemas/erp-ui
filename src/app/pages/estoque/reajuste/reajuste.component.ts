import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { EstoqueReajusteCabecalho } from 'src/app/classes/estoque-reajuste-cabecalho';
import { EstoqueReajusteService } from 'src/app/servicos/estoque-reajuste.service';
import { VariaveisGlobais } from 'src/app/classes/variaveis-globais';

@Component({
  selector: 'app-reajuste',
  templateUrl: './reajuste.component.html',
  styleUrls: ['./reajuste.component.css'],
  styles: [`
  /* Column Priorities */
  @media only all {
      th.ui-p-6,
      td.ui-p-6,
      th.ui-p-5,
      td.ui-p-5,
      th.ui-p-4,
      td.ui-p-4,
      th.ui-p-3,
      td.ui-p-3,
      th.ui-p-2,
      td.ui-p-2,
      th.ui-p-1,
      td.ui-p-1 {
          display: none;
      }
  }

  /* Show priority 1 at 320px (20em x 16px) */
  @media screen and (min-width: 20em) {
      th.ui-p-1,
      td.ui-p-1 {
          display: table-cell;
      }
  }

  /* Show priority 2 at 480px (30em x 16px) */
  @media screen and (min-width: 30em) {
      th.ui-p-2,
      td.ui-p-2 {
          display: table-cell;
      }
  }

  /* Show priority 3 at 640px (40em x 16px) */
  @media screen and (min-width: 40em) {
      th.ui-p-3,
      td.ui-p-3 {
          display: table-cell;
      }
  }

  /* Show priority 4 at 800px (50em x 16px) */
  @media screen and (min-width: 50em) {
      th.ui-p-4,
      td.ui-p-4 {
          display: table-cell;
      }
  }

  /* Show priority 5 at 960px (60em x 16px) */
  @media screen and (min-width: 60em) {
      th.ui-p-5,
      td.ui-p-5 {
          display: table-cell;
      }
  }

  /* Show priority 6 at 1,120px (70em x 16px) */
  @media screen and (min-width: 70em) {
      th.ui-p-6,
      td.ui-p-6 {
          display: table-cell;
      }
  }
`]
})
export class ReajusteComponent implements OnInit {

  listaEstoqueReajuste: EstoqueReajusteCabecalho[];
  cols: any[];
  estoqueReajusteSelecionado: EstoqueReajusteCabecalho;
  ptBR: any;

  constructor(private estoqueReajusteService: EstoqueReajusteService,
    private confirmationService: ConfirmationService,
    private global: VariaveisGlobais) {
      this.global.botaoDesabilitado = true;
    }

  ngOnInit() {
    this.global.tituloJanela = 'Lista de Reajustes';
    this.ptBR = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
                    'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };
  }

  consultarReajuste() {
    this.carregaDados();
  }

  private carregaDados() {
    this.estoqueReajusteService.getListaEstoqueReajusteCabecalho().subscribe(
      lista => {
        this.listaEstoqueReajuste = lista;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Ocorreu um erro', this.global.tratarError(error));
      }
    );
  }

  getEditar(obj: EstoqueReajusteCabecalho) {
    return obj.id;
  }

  getExcluir(obj: EstoqueReajusteCabecalho) {
    this.confirmationService.confirm({
      message: 'Excluir o registro selecionado?',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.estoqueReajusteService.getExcluir(obj.id).subscribe(
          _ => {
            this.carregaDados();
            this.global.mostraMensagem(this.global.info, 'Confirmação', 'Registro excluído com sucesso');
            this.global.botaoDesabilitado = true;
          },
          error => {
            this.global.mostraMensagem(this.global.error, 'Ocorreu um erro', this.global.tratarError(error));
          }
        );
      },
      reject: () => {
        // this.msgs = [{ severity: 'info', summary: 'Rejected', detail: 'You have rejected' }];
      }
    });
  }
}
