import { EstoqueContagemCabecalho } from './../../../classes/estoque-contagem-cabecalho';
import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { EstoqueReajusteCabecalho } from 'src/app/classes/estoque-reajuste-cabecalho';
import { Colaborador } from 'src/app/classes/colaborador';
import { FormGroup } from '@angular/forms';
import { Produto } from 'src/app/classes/produto';
import { EstoqueReajusteService } from 'src/app/servicos/estoque-reajuste.service';
import { ColaboradorService } from 'src/app/servicos/colaborador.service';
import { ProdutoService } from 'src/app/servicos/produto.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { VariaveisGlobais } from 'src/app/classes/variaveis-globais';
import { EstoqueReajusteDetalhe } from 'src/app/classes/estoque-reajuste-detalhe';

@Component({
  selector: 'app-reajuste-detalhe',
  templateUrl: './reajuste-detalhe.component.html',
  styleUrls: ['./reajuste-detalhe.component.css']
})
export class ReajusteDetalheComponent implements OnInit {

  estoqueReajuste: EstoqueReajusteCabecalho;
  filtroColaborador: Colaborador[];
  filtroProduto: Produto[];
  produto: Produto;
  ptBR: any;
  display: boolean;
  opcoesTipoReajuste: SelectItem[];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private locate: Location,
    private produtoService: ProdutoService,
    private estoqueReajusteService: EstoqueReajusteService,
    private colaboradorService: ColaboradorService,
    private global: VariaveisGlobais) {
    this.display = false;
  }

  ngOnInit() {
    this.global.tituloJanela = null;
    this.global.tituloJanela = 'Detalhe Reajuste';
    this.estoqueReajuste = new EstoqueReajusteCabecalho();
    this.global.botaoDesabilitado = true;

    this.opcoesTipoReajuste = [
      {label: 'Aumentar', value: 'A'},
      {label: 'Diminuir', value: 'D'}
    ];

    this.ptBR = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
        'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };

    if (this.router.url !== '/estoque-reajuste/novo') {
      this.global.tituloJanela = null;
      this.global.tituloJanela = 'Editando Reajuste';
      const id = this.route.snapshot.paramMap.get('id');
      this.estoqueReajusteService.getEstoqueReajusteCabecalho(parseInt(id, 0)).subscribe(
        dados => {
          this.estoqueReajuste = dados;
        },
        error => {
          this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
        }
      );
    }
  }

  cancelar() {
    this.locate.back();
  }

  salvar() {

    if (this.estoqueReajuste.listaEstoqueReajusteDetalhe === undefined ||
      this.estoqueReajuste.listaEstoqueReajusteDetalhe === []) {
        this.global.mostraMensagem(this.global.warn, 'Atenção', 'Inserir pelo menos 1 item para reajuste!');
        return;
    }

    this.estoqueReajusteService.getSalvar(this.estoqueReajuste).subscribe(
      dados => {
        this.estoqueReajuste = dados;
        this.global.mostraMensagem(this.global.info, 'Sucesso!', 'Reajuste salvo com Sucesso');
        this.cancelar();
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
    this.estoqueReajuste = new EstoqueReajusteCabecalho();
  }

  buscaColaborador(event) {
    this.colaboradorService.getListaColaboradores(event.query).subscribe(
      dados => {
        this.filtroColaborador = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaProduto(event) {
    this.produtoService.getListaProdutos(event.query).subscribe(
      dados => {
        this.filtroProduto = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  inserir() {
    this.display = true;
  }

  incluirProduto() {
    if (this.estoqueReajuste.listaEstoqueReajusteDetalhe === undefined) {
      this.estoqueReajuste.listaEstoqueReajusteDetalhe = [];
    }
    const detalhe: EstoqueReajusteDetalhe = new EstoqueReajusteDetalhe();
    detalhe.produto = this.produto;
    this.estoqueReajuste.listaEstoqueReajusteDetalhe.push(detalhe);

    this.display = false;
  }

  cancelarInclusao() {
    this.estoqueReajuste = new EstoqueReajusteCabecalho();
    this.display = false;
  }
}
