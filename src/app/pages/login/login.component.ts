import { VariaveisGlobais } from './../../classes/variaveis-globais';
import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../classes/usuario';
import { LoginService } from '../../servicos/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: Usuario;

  constructor(private loginService: LoginService,
    private global: VariaveisGlobais,
    private router: Router) {
  }

  ngOnInit() {
    this.usuario = new Usuario();
  }

  login() {
    this.global.loading = true;
    this.loginService.loginIn(this.usuario)
    .then(() => {
      this.router.navigate(['/']);
    })
    .catch(error => {
      this.global.mostraMensagem(this.global.error, 'Atenção', error);
    });
    this.global.loading = false;
  }

}
