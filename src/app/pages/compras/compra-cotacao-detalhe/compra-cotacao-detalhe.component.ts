import { CompraReqCotacaoDetalhe } from './../../../classes/compra-req-cotacao-detalhe';
import { CompraRequisicao } from 'src/app/classes/compra-requisicao';
import { CompraRequisicaoDetalhe } from './../../../classes/compra-requisicao-detalhe';
import { CompraFornecedorCotacao } from './../../../classes/compra-fornecedor-cotacao';
import { FornecedorService } from './../../../servicos/fornecedor.service';
import { Fornecedor } from './../../../classes/fornecedor';
import { Location } from '@angular/common';
import { VariaveisGlobais } from './../../../classes/variaveis-globais';
import { CompraCotacaoService } from './../../../servicos/compra-cotacao.service';
import { CompraCotacao } from './../../../classes/compra-cotacao';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProdutoService } from 'src/app/servicos/produto.service';
import { Produto } from 'src/app/classes/produto';
import { CompraRequisicaoService } from 'src/app/servicos/compra-requisicao.service';

@Component({
  selector: 'app-compra-cotacao-detalhe',
  templateUrl: './compra-cotacao-detalhe.component.html',
  styleUrls: ['./compra-cotacao-detalhe.component.css']
})
export class CompraCotacaoDetalheComponent implements OnInit {

  compraCotacao: CompraCotacao;
  compraCotacaoSelecionado: CompraCotacao;
  filtroFornecedor: Fornecedor[];
  filtroProduto: Produto[];
  fornecedor: Fornecedor;
  produto: Produto;
  ptBR: any;
  displayFornec: boolean;
  displayProd: boolean;
  requisicaoSelecionada:  CompraRequisicaoDetalhe;
  listaCompraRequisicaoDetalhe: CompraRequisicaoDetalhe[];
  quantidade: number;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private locate: Location,
    private compraCotacaoService: CompraCotacaoService,
    private fornecedorService: FornecedorService,
    private produtoService: ProdutoService,
    private requisicaoService: CompraRequisicaoService,
    private global: VariaveisGlobais) {
    this.displayFornec = false;
  }

  ngOnInit() {
    this.global.tituloJanela = null;
    this.global.tituloJanela = 'Detalhe Compra Cotação';
    this.compraCotacao = new CompraCotacao();
    this.global.botaoDesabilitado = true;

    this.ptBR = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
        'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };

    if (this.router.url !== '/compra-cotacao/novo') {
      this.global.tituloJanela = null;
      this.global.tituloJanela = 'Editando Compra Cotação';
      const id = this.route.snapshot.paramMap.get('id');
      this.compraCotacaoService.getCompraCotacao(parseInt(id, 0)).subscribe(
        dados => {
          this.compraCotacao = dados;
          console.log(this.compraCotacao);
        },
        error => {
          this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
        }
      );
    }
  }

  cancelar() {
    this.locate.back();
  }

  salvar() {
    this.compraCotacaoService.getSalvar(this.compraCotacao).subscribe(
      dados => {
        this.compraCotacao = dados;
        this.global.mostraMensagem(this.global.info, 'Sucesso!',
          this.compraCotacao == null ? 'Cotação salvo com Sucesso' : 'Cotação alterado com Sucesso');
        this.cancelar();
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
    this.compraCotacao = new CompraCotacao();
  }

  buscaFornecedor(event) {
    this.fornecedorService.getListaFornecedores(event.query).subscribe(
      dados => {
        this.filtroFornecedor = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  incluirFornecedor() {
    const compraFornecedorCotacao: CompraFornecedorCotacao = new CompraFornecedorCotacao();
    compraFornecedorCotacao.fornecedor = this.fornecedor;
    this.compraCotacao.listaCompraFornecedorCotacao.push(compraFornecedorCotacao);
    this.displayFornec = false;
  }

  cancelarInclusaoFornec() {
    this.displayFornec = false;
  }

  novoFornecedor() {
    this.fornecedor = new Fornecedor();
    this.displayFornec = true;
  }

  buscaProduto(event) {
    this.produtoService.getListaProdutos(event.query).subscribe(
      dados => {
        this.filtroProduto = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  novoItem() {
    this.displayProd = true;
    this.requisicaoService.getListaCompraRequisicaoDetalhe().subscribe(
      dados => {
        this.listaCompraRequisicaoDetalhe  = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  cancelarInclusaoProd() {
    this.displayFornec = false;
  }

  incluirProduto() {
    const reqCotacaoDetalhe: CompraReqCotacaoDetalhe = new CompraReqCotacaoDetalhe();
    reqCotacaoDetalhe.compraRequisicaoDetalhe = this.requisicaoSelecionada;
    reqCotacaoDetalhe.quantidadeCotada = this.quantidade;
    this.compraCotacao.listaCompraReqCotacaoDetalhe.push(reqCotacaoDetalhe);
    this.displayProd = false;
  }

}
