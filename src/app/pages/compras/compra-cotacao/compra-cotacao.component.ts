import { VariaveisGlobais } from './../../../classes/variaveis-globais';
import { ConfirmationService } from 'primeng/api';
import { CompraCotacaoService } from './../../../servicos/compra-cotacao.service';
import { CompraCotacao } from './../../../classes/compra-cotacao';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-compra-cotacao',
  templateUrl: './compra-cotacao.component.html',
  styleUrls: ['./compra-cotacao.component.css'],
  styles: [`
  /* Column Priorities */
  @media only all {
      th.ui-p-6,
      td.ui-p-6,
      th.ui-p-5,
      td.ui-p-5,
      th.ui-p-4,
      td.ui-p-4,
      th.ui-p-3,
      td.ui-p-3,
      th.ui-p-2,
      td.ui-p-2,
      th.ui-p-1,
      td.ui-p-1 {
          display: none;
      }
  }

  /* Show priority 1 at 320px (20em x 16px) */
  @media screen and (min-width: 20em) {
      th.ui-p-1,
      td.ui-p-1 {
          display: table-cell;
      }
  }

  /* Show priority 2 at 480px (30em x 16px) */
  @media screen and (min-width: 30em) {
      th.ui-p-2,
      td.ui-p-2 {
          display: table-cell;
      }
  }

  /* Show priority 3 at 640px (40em x 16px) */
  @media screen and (min-width: 40em) {
      th.ui-p-3,
      td.ui-p-3 {
          display: table-cell;
      }
  }

  /* Show priority 4 at 800px (50em x 16px) */
  @media screen and (min-width: 50em) {
      th.ui-p-4,
      td.ui-p-4 {
          display: table-cell;
      }
  }

  /* Show priority 5 at 960px (60em x 16px) */
  @media screen and (min-width: 60em) {
      th.ui-p-5,
      td.ui-p-5 {
          display: table-cell;
      }
  }

  /* Show priority 6 at 1,120px (70em x 16px) */
  @media screen and (min-width: 70em) {
      th.ui-p-6,
      td.ui-p-6 {
          display: table-cell;
      }
  }
`]
})
export class CompraCotacaoComponent implements OnInit {

  listaCompraCotacao: CompraCotacao[];
  compraCotacaoSelecionado: CompraCotacao;
  colunas: any[];
  ptBR: any;

  constructor(private compraCotacaoService: CompraCotacaoService,
    private confirmationService: ConfirmationService,
    private global: VariaveisGlobais) {
      this.global.tituloJanela = '';
      this.global.botaoDesabilitado = true;
    }

  ngOnInit() {
    this.ptBR = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
                    'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };
    this.global.tituloJanela = 'Lista de Cotações';
    this.compraCotacaoSelecionado = new CompraCotacao();
  }

  consultar() {
    this.carregaDados();
  }

  getEditar(obj: CompraCotacao) {
    return obj.id;
  }

  getExcluir(obj: CompraCotacao) {
    this.confirmationService.confirm({
      message: 'Excluir o registro selecionado?',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.compraCotacaoService.getRemover(obj.id).subscribe(
          dados => {
            this.carregaDados();
            this.global.mostraMensagem(this.global.info, 'Confirmar', 'Cotação Excluido!');
            this.global.botaoDesabilitado = true;
          },
          error => {
            this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
          }
        );
      },
      reject: () => {
      }
    });
  }

  private carregaDados() {
    this.global.loading = true;
    this.compraCotacaoService.getCompraCotacoes().subscribe(
      dados => {
        this.listaCompraCotacao = dados;
        this.global.loading = false;
        console.log(this.listaCompraCotacao);
      },
      error => {
        this.global.loading = false;
        this.global.mostraMensagem(this.global.error, 'Atenção', this.global.tratarError(error));
      }
    );
  }

}
