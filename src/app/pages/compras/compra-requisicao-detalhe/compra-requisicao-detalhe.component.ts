import { ProdutoService } from './../../../servicos/produto.service';
import { Produto } from './../../../classes/produto';
import { CompraRequisicaoDetalhe } from './../../../classes/compra-requisicao-detalhe';
import { Location } from '@angular/common';
import { VariaveisGlobais } from './../../../classes/variaveis-globais';
import { CompraTipoRequisicaoService } from './../../../servicos/compra-tipo-requisicao.service';
import { CompraRequisicaoService } from './../../../servicos/compra-requisicao.service';
import { Colaborador } from './../../../classes/colaborador';
import { CompraTipoRequisicao } from './../../../classes/compra-tipo-requisicao';
import { Component, OnInit } from '@angular/core';
import { CompraRequisicao } from 'src/app/classes/compra-requisicao';
import { ActivatedRoute, Router } from '@angular/router';
import { ColaboradorService } from 'src/app/servicos/colaborador.service';

@Component({
  selector: 'app-compra-requisicao-detalhe',
  templateUrl: './compra-requisicao-detalhe.component.html',
  styleUrls: ['./compra-requisicao-detalhe.component.css']
})
export class CompraRequisicaoDetalheComponent implements OnInit {

  compraRequisicao: CompraRequisicao;
  compraRequisicaoSelecionado: CompraRequisicao;
  filtroTipoRequisicao: CompraTipoRequisicao[];
  filtroColaborador: Colaborador[];
  detalheSelecionado: CompraRequisicaoDetalhe;
  filtroProduto: Produto[];
  ptBR: any;
  display: boolean;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private locate: Location,
    private produtoService: ProdutoService,
    private compraRequisicaoService: CompraRequisicaoService,
    private tipoRequisicaoService: CompraTipoRequisicaoService,
    private colaboradorService: ColaboradorService,
    private global: VariaveisGlobais) {
    this.display = false;
  }

  ngOnInit() {
    this.global.tituloJanela = null;
    this.global.tituloJanela = 'Detalhe Compra Requisicao';
    this.compraRequisicao = new CompraRequisicao();
    this.detalheSelecionado = new CompraRequisicaoDetalhe();
    this.global.botaoDesabilitado = true;

    this.ptBR = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
        'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };

    if (this.router.url !== '/compra-requisicao/novo') {
      this.global.tituloJanela = null;
      this.global.tituloJanela = 'Editando Compra Requisicao';
      const id = this.route.snapshot.paramMap.get('id');
      this.compraRequisicaoService.getCompraRequisicao(parseInt(id, 0)).subscribe(
        dados => {
          this.compraRequisicao = dados;
        },
        error => {
          this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
        }
      );
    }
  }

  cancelar() {
    this.locate.back();
  }

  salvar() {
    this.compraRequisicaoService.getSalvar(this.compraRequisicao).subscribe(
      dados => {
        this.compraRequisicao = dados;
        this.global.mostraMensagem(this.global.info, 'Sucesso!',
          this.compraRequisicao == null ? 'Requisição salvo com Sucesso' : 'Requisição alterado com Sucesso');
        this.cancelar();
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
    this.compraRequisicao = new CompraRequisicao();
  }

  getEditar() {
    return this.compraRequisicaoSelecionado == null ? null : this.compraRequisicaoSelecionado.id;
  }

  buscaTipoRequisicao(event) {
    this.tipoRequisicaoService.getListaCompraTipoRequisicao(event.query).subscribe(
      dados => {
        this.filtroTipoRequisicao = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaColaborador(event) {
    this.colaboradorService.getListaColaboradores(event.query).subscribe(
      dados => {
        this.filtroColaborador = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaProduto(event) {
    this.produtoService.getListaProdutos(event.query).subscribe(
      dados => {
        this.filtroProduto = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  inserir() {
    if (this.compraRequisicao.listaCompraRequisicaoDetalhe === undefined) {
      this.compraRequisicao.listaCompraRequisicaoDetalhe = [];
    }
    this.detalheSelecionado = new CompraRequisicaoDetalhe();
    this.display = true;
  }

  cancelarInclusao() {
    this.detalheSelecionado = new CompraRequisicaoDetalhe();
    this.display = false;
  }

  incluirProduto() {
    if (this.detalheSelecionado.id == null) {
      this.detalheSelecionado.quantidadeCotada = 0;
      this.detalheSelecionado.itemCotado = 'N';
      this.compraRequisicao.listaCompraRequisicaoDetalhe.push(this.detalheSelecionado);
      this.detalheSelecionado = new CompraRequisicaoDetalhe();
    }
    this.display = false;
  }

  editar(obj: CompraRequisicaoDetalhe) {
    this.detalheSelecionado = obj;
    this.display = true;
  }

  excluirProduto(obj: CompraRequisicaoDetalhe) {
    if (obj.itemCotado === 'S') {
      this.global.mostraMensagem(this.global.warn, 'Atenção', 'Item já cotado! Não pode ser removido!');
    } else {
      const index = this.compraRequisicao.listaCompraRequisicaoDetalhe.indexOf(obj, 1);
      this.compraRequisicao.listaCompraRequisicaoDetalhe.splice(index, 1);
    }
  }
}
