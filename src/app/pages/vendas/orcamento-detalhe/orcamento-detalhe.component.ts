import { TransportadoraService } from './../../../servicos/transportadora.service';
import { ProdutoService } from './../../../servicos/produto.service';
import { VariaveisGlobais } from 'src/app/classes/variaveis-globais';
import { VendedorService } from './../../../servicos/vendedor.service';
import { ClienteService } from './../../../servicos/cliente.service';
import { VendaCondicoesPagamentoService } from './../../../servicos/venda-condicoes-pagamento.service';
import { VendaOrcamentoService } from './../../../servicos/venda-orcamento.service';
import { VendaCondicoesPagamento } from './../../../classes/venda-condicoes-pagamento';
import { VendaOrcamentoCabecalho } from './../../../classes/venda-orcamento-cabecalho';
import { Component, OnInit } from '@angular/core';
import { Transportadora } from 'src/app/classes/transportadora';
import { Cliente } from 'src/app/classes/cliente';
import { Vendedor } from 'src/app/classes/vendedor';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Produto } from 'src/app/classes/produto';
import { VendaOrcamentoDetalhe } from 'src/app/classes/venda-orcamento-detalhe';

@Component({
  selector: 'app-orcamento-detalhe',
  templateUrl: './orcamento-detalhe.component.html',
  styleUrls: ['./orcamento-detalhe.component.css']
})
export class OrcamentoDetalheComponent implements OnInit {

  orcamento: VendaOrcamentoCabecalho;
  condicaoPagamento: VendaCondicoesPagamento;
  filtroCondicaoPagamento: VendaCondicoesPagamento[];
  transportadora: Transportadora;
  filtroTransportadora: Transportadora[];
  cliente: Cliente;
  filtroCliente: Cliente[];
  vendedor: Vendedor;
  filtroVendedor: Vendedor[];
  produto: Produto;
  filtroProduto: Produto[];
  ptBR: any;
  displayFornec: boolean;
  displayProd: boolean;
  qtdeProduto: number;
  valorProduto: number;
  valorDesconto: number;
  quantidade: number;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private locate: Location,
    private orcamentoService: VendaOrcamentoService,
    private condicaoPagamentoService: VendaCondicoesPagamentoService,
    private clienteService: ClienteService,
    private vendedorService: VendedorService,
    private produtoService: ProdutoService,
    private transportadoraService: TransportadoraService,
    private global: VariaveisGlobais) {
    this.displayFornec = false;
  }

  ngOnInit() {
    this.global.tituloJanela = 'Detalhe Orçamento';
    this.orcamento = new VendaOrcamentoCabecalho();
    this.orcamento.valorTotal = 0;
    this.global.botaoDesabilitado = true;

    this.qtdeProduto = 0;
    this.valorProduto = 0;
    this.valorDesconto = 0;

    this.ptBR = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
        'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };

    if (this.router.url !== '/venda-orcamento/novo') {
      this.global.tituloJanela = null;
      this.global.tituloJanela = 'Editando Orçamento';
      const id = this.route.snapshot.paramMap.get('id');
      this.orcamentoService.getVendaOrcamentoCabecalho(parseInt(id, 0)).subscribe(
        dados => {
          this.orcamento = dados;
          console.log(this.orcamento);
        },
        error => {
          this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
        }
      );
    }
  }

  cancelar() {
    this.locate.back();
  }

  salvar() {
    this.orcamentoService.salvar(this.orcamento).subscribe(
      dados => {
        this.orcamento = dados;
        this.global.mostraMensagem(this.global.info, 'Sucesso!',
          this.orcamento.id == null ? 'Orçamento salvo com Sucesso' : 'Orçamento alterado com Sucesso');
        this.cancelar();
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
    this.orcamento = new VendaOrcamentoCabecalho();
  }

  buscaCondicaoPagamento(event) {
    this.condicaoPagamentoService.getListaVendaCondicoesPagamento(event.query).subscribe(
      dados => {
        this.filtroCondicaoPagamento = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaVendedor(event) {
    this.vendedorService.getListaVendedor(event.query).subscribe(
      dados => {
        this.filtroVendedor = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaCliente(event) {
    this.clienteService.getListaClientes(event.query).subscribe(
      dados => {
        this.filtroCliente = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaProduto(event) {
    this.produtoService.getListaProdutos(event.query).subscribe(
      dados => {
        this.filtroProduto = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaTransportadora(event) {
    this.transportadoraService.getListaTransportadoras(event.query).subscribe(
      dados => {
        this.filtroTransportadora = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  selecionarProduto(event) {
    this.valorProduto = event.valorVenda;
  }

  novoProduto() {
    const itemDetalhe: VendaOrcamentoDetalhe = new VendaOrcamentoDetalhe();
    itemDetalhe.produto = this.produto;
    itemDetalhe.quantidade = this.qtdeProduto;
    itemDetalhe.valorUnitario = this.valorProduto;
    itemDetalhe.valorDesconto = this.valorDesconto;
    itemDetalhe.valorTotal = (this.qtdeProduto * this.valorProduto) - this.valorDesconto;
    this.orcamento.valorTotal += itemDetalhe.valorTotal;
    this.orcamento.listaVendaOrcamentoDetalhe.push(itemDetalhe);
    this.produto = new Produto();
    this.qtdeProduto = 0;
    this.valorProduto = 0;
  }
}
