import { TransportadoraService } from './../../../servicos/transportadora.service';
import { TipoNotaFiscal } from './../../../classes/tipo-nota-fiscal';
import { VendaDetalhe } from './../../../classes/venda-detalhe';
import { Location } from '@angular/common';
import { VariaveisGlobais } from './../../../classes/variaveis-globais';
import { ProdutoService } from './../../../servicos/produto.service';
import { VendedorService } from './../../../servicos/vendedor.service';
import { ClienteService } from './../../../servicos/cliente.service';
import { VendaCondicoesPagamentoService } from './../../../servicos/venda-condicoes-pagamento.service';
import { VendaCabecalhoService } from './../../../servicos/venda-cabecalho.service';
import { VendaOrcamentoService } from './../../../servicos/venda-orcamento.service';
import { ActivatedRoute, Router } from '@angular/router';
import { VendaOrcamentoCabecalho } from './../../../classes/venda-orcamento-cabecalho';
import { Produto } from 'src/app/classes/produto';
import { Vendedor } from 'src/app/classes/vendedor';
import { Cliente } from './../../../classes/cliente';
import { Transportadora } from './../../../classes/transportadora';
import { VendaCondicoesPagamento } from './../../../classes/venda-condicoes-pagamento';
import { VendaCabecalho } from './../../../classes/venda-cabecalho';
import { Component, OnInit } from '@angular/core';
import { TipoNotaFiscalService } from 'src/app/servicos/tipo-nota-fiscal.service';

@Component({
  selector: 'app-pedido-detalhe',
  templateUrl: './pedido-detalhe.component.html',
  styleUrls: ['./pedido-detalhe.component.css']
})
export class PedidoDetalheComponent implements OnInit {

  pedido: VendaCabecalho;
  condicaoPagamento: VendaCondicoesPagamento;
  filtroCondicaoPagamento: VendaCondicoesPagamento[];
  transportadora: Transportadora;
  filtroTransportadora: Transportadora[];
  cliente: Cliente;
  filtroCliente: Cliente[];
  vendedor: Vendedor;
  filtroVendedor: Vendedor[];
  produto: Produto;
  filtroProduto: Produto[];
  orcamento: VendaOrcamentoCabecalho;
  filtroOrcamento: VendaOrcamentoCabecalho[];
  tipoFiscal: TipoNotaFiscal;
  filtroTipoFiscal: TipoNotaFiscal[];
  ptBR: any;
  displayFornec: boolean;
  displayProd: boolean;
  qtdeProduto: number;
  valorProduto: number;
  valorDesconto: number;
  quantidade: number;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private locate: Location,
    private pedidoService: VendaCabecalhoService,
    private orcamentoService: VendaOrcamentoService,
    private condicaoPagamentoService: VendaCondicoesPagamentoService,
    private tipoFiscalService: TipoNotaFiscalService,
    private clienteService: ClienteService,
    private vendedorService: VendedorService,
    private produtoService: ProdutoService,
    private trasportadoraService: TransportadoraService,
    private global: VariaveisGlobais) {
    this.displayFornec = false;
  }

  ngOnInit() {
    this.global.tituloJanela = 'Detalhe Pedido';
    this.pedido = new VendaCabecalho();
    this.pedido.valorTotal = 0;
    this.global.botaoDesabilitado = true;

    this.qtdeProduto = 0;
    this.valorProduto = 0;
    this.valorDesconto = 0;

    this.ptBR = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
        'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };

    if (this.router.url !== '/venda-pedido/novo') {
      this.global.tituloJanela = null;
      this.global.tituloJanela = 'Editando Pedido';
      const id = this.route.snapshot.paramMap.get('id');
      this.pedidoService.getVendaCabecalho(parseInt(id, 0)).subscribe(
        dados => {
          this.pedido = dados;
          console.log(this.pedido);
        },
        error => {
          this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
        }
      );
    }
  }

  cancelar() {
    this.locate.back();
  }

  salvar() {
    this.pedidoService.salvar(this.pedido).subscribe(
      dados => {
        this.pedido = dados;
        this.global.mostraMensagem(this.global.info, 'Sucesso!',
          this.pedido.id == null ? 'Pedido salvo com Sucesso' : 'Pedido alterado com Sucesso');
        this.cancelar();
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
    this.pedido = new VendaCabecalho();
  }

  buscaCondicaoPagamento(event) {
    this.condicaoPagamentoService.getListaVendaCondicoesPagamento(event.query).subscribe(
      dados => {
        this.filtroCondicaoPagamento = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaVendedor(event) {
    this.vendedorService.getListaVendedor(event.query).subscribe(
      dados => {
        this.filtroVendedor = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaCliente(event) {
    this.clienteService.getListaClientes(event.query).subscribe(
      dados => {
        this.filtroCliente = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaProduto(event) {
    this.produtoService.getListaProdutos(event.query).subscribe(
      dados => {
        this.filtroProduto = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaOrcamento(event) {
    this.orcamentoService.getListaVendaOrcamentoCabecalho(event.query).subscribe(
      dados => {
        this.filtroOrcamento = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaTipoNotaFiscal(event) {
    this.tipoFiscalService.getListaTipoNotaFiscal(event.query).subscribe(
      dados => {
        this.filtroTipoFiscal = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  buscaTransportadora(event) {
    this.trasportadoraService.getListaTransportadoras(event.query).subscribe(
      dados => {
        this.filtroTransportadora = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
      }
    );
  }

  selecionarProduto(event) {
    this.valorProduto = event.valorVenda;
  }

  novoProduto() {
    if (this.qtdeProduto == null || this.qtdeProduto === 0) {
      this.global.mostraMensagem(this.global.warn, 'Atenção', 'informe a quantidade do Item!');
    } else {
      const itemDetalhe: VendaDetalhe = new VendaDetalhe();
      itemDetalhe.produto = this.produto;
      itemDetalhe.quantidade = this.qtdeProduto;
      itemDetalhe.valorUnitario = this.valorProduto;
      itemDetalhe.valorDesconto = this.valorDesconto;
      itemDetalhe.valorTotal = (this.qtdeProduto * this.valorProduto);
      this.pedido.valorTotal += itemDetalhe.valorTotal;
      this.pedido.listaVendaDetalhe.push(itemDetalhe);
      this.produto = new Produto();
      this.qtdeProduto = 0;
      this.valorProduto = 0;
    }
  }

}
