import { VendaCabecalhoService } from './../../../servicos/venda-cabecalho.service';
import { Component, OnInit } from '@angular/core';
import { VendaCabecalho } from 'src/app/classes/venda-cabecalho';
import { ConfirmationService } from 'primeng/api';
import { VariaveisGlobais } from 'src/app/classes/variaveis-globais';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.css']
})
export class PedidoComponent implements OnInit {

  listaPedido: VendaCabecalho[];
  pedidoSelecionado: VendaCabecalho;
  colunas: any[];
  ptBR: any;

  constructor(private pedidoService: VendaCabecalhoService,
    private confirmationService: ConfirmationService,
    private global: VariaveisGlobais) {
      this.global.botaoDesabilitado = true;
    }

  ngOnInit() {
    this.ptBR = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
                    'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };
    this.global.tituloJanela = 'Lista de Pedidos';
    this.pedidoSelecionado = new VendaCabecalho();
  }

  consultar() {
    this.carregaDados();
  }

  getEditar(obj: VendaCabecalho) {
    return obj.id;
  }

  getExcluir(obj: VendaCabecalho) {
    this.confirmationService.confirm({
      message: 'Excluir o registro selecionado?',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.pedidoService.excluir(obj.id).subscribe(
          dados => {
            this.carregaDados();
            this.global.mostraMensagem(this.global.info, 'Confirmar', 'Pedido Excluido!');
            this.global.botaoDesabilitado = true;
          },
          error => {
            this.global.mostraMensagem(this.global.error, 'Atenção', 'Dados Inconsistentes!: ' + this.global.tratarError(error));
          }
        );
      },
      reject: () => {
      }
    });
  }

  private carregaDados() {
    this.pedidoService.getListaVendaCabecalho().subscribe(
      dados => {
        this.listaPedido = dados;
      },
      error => {
        this.global.mostraMensagem(this.global.error, 'Atenção', this.global.tratarError(error));
      }
    );
  }

}
