import { HttpModule, Http, RequestOptions } from '@angular/http';
import { ParcelaPagamentoDetalheComponent } from './pages/financeiro/parcela-pagamento-detalhe/parcela-pagamento-detalhe.component';
import { ParcelaPagamentoComponent } from './pages/financeiro/parcela-pagamento/parcela-pagamento.component';
import { LancamentoPagarComponent } from './pages/financeiro/lancamento-pagar/lancamento-pagar.component';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';

// Import PrimeNG modules
import { AccordionModule } from 'primeng/accordion';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CarouselModule } from 'primeng/carousel';
import { CardModule } from 'primeng/card';
import { ChartModule } from 'primeng/chart';
import { CheckboxModule } from 'primeng/checkbox';
import { ChipsModule } from 'primeng/chips';
import { CodeHighlighterModule } from 'primeng/codehighlighter';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ColorPickerModule } from 'primeng/colorpicker';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DataViewModule } from 'primeng/dataview';
import { DataScrollerModule } from 'primeng/datascroller';
import { DialogModule } from 'primeng/dialog';
import { DragDropModule } from 'primeng/dragdrop';
import { DropdownModule } from 'primeng/dropdown';
import { EditorModule } from 'primeng/editor';
import { FieldsetModule } from 'primeng/fieldset';
import { FileUploadModule } from 'primeng/fileupload';
import { GalleriaModule } from 'primeng/galleria';
import { GMapModule } from 'primeng/gmap';
import { GrowlModule } from 'primeng/growl';
import { InplaceModule } from 'primeng/inplace';
import { InputMaskModule } from 'primeng/inputmask';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { LightboxModule } from 'primeng/lightbox';
import { ListboxModule } from 'primeng/listbox';
import { MegaMenuModule } from 'primeng/megamenu';
import { MenuModule } from 'primeng/menu';
import { MenubarModule } from 'primeng/menubar';
import { MessagesModule } from 'primeng/messages';
import { MultiSelectModule } from 'primeng/multiselect';
import { OrderListModule } from 'primeng/orderlist';
import { OrganizationChartModule } from 'primeng/organizationchart';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { PaginatorModule } from 'primeng/paginator';
import { PanelModule } from 'primeng/panel';
import { PanelMenuModule } from 'primeng/panelmenu';
import { PasswordModule } from 'primeng/password';
import { PickListModule } from 'primeng/picklist';
import { ProgressBarModule } from 'primeng/progressbar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { RatingModule } from 'primeng/rating';
import { ScheduleModule } from 'primeng/schedule';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { SelectButtonModule } from 'primeng/selectbutton';
import { SlideMenuModule } from 'primeng/slidemenu';
import { SliderModule } from 'primeng/slider';
import { SpinnerModule } from 'primeng/spinner';
import { SplitButtonModule } from 'primeng/splitbutton';
import { StepsModule } from 'primeng/steps';
import { TabMenuModule } from 'primeng/tabmenu';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { TerminalModule } from 'primeng/terminal';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { ToastModule } from 'primeng/toast';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { ToolbarModule } from 'primeng/toolbar';
import { TooltipModule } from 'primeng/tooltip';
import { TreeModule } from 'primeng/tree';
import { TreeTableModule } from 'primeng/treetable';

import { ArmazemComponent } from './pages/cadastros/armazem/armazem.component';
import { AppRoutingModule } from './/app-routing.module';
import { ArmazemDetalheComponent } from './pages/cadastros/armazem-detalhe/armazem-detalhe.component';
import { NaoEncontradoComponent } from './pages/error/nao-encontrado/nao-encontrado.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { EmpresaComponent } from './pages/cadastros/empresa/empresa.component';
import { EmpresaDetalheComponent } from './pages/cadastros/empresa-detalhe/empresa-detalhe.component';
import { CargoComponent } from './pages/cadastros/cargo/cargo.component';
import { CargoDetalheComponent } from './pages/cadastros/cargo-detalhe/cargo-detalhe.component';
import { ColaboradorComponent } from './pages/cadastros/colaborador/colaborador.component';
import { ColaboradorDetalheComponent } from './pages/cadastros/colaborador-detalhe/colaborador-detalhe.component';
import { CompraRequisicaoComponent } from './pages/compras/compra-requisicao/compra-requisicao.component';
import { CompraRequisicaoDetalheComponent } from './pages/compras/compra-requisicao-detalhe/compra-requisicao-detalhe.component';
import { CompraCotacaoComponent } from './pages/compras/compra-cotacao/compra-cotacao.component';
import { CompraCotacaoDetalheComponent } from './pages/compras/compra-cotacao-detalhe/compra-cotacao-detalhe.component';
import { OrcamentoComponent } from './pages/vendas/orcamento/orcamento.component';
import { OrcamentoDetalheComponent } from './pages/vendas/orcamento-detalhe/orcamento-detalhe.component';
import { PedidoComponent } from './pages/vendas/pedido/pedido.component';
import { PedidoDetalheComponent } from './pages/vendas/pedido-detalhe/pedido-detalhe.component';
import { ReajusteComponent } from './pages/estoque/reajuste/reajuste.component';
import { ReajusteDetalheComponent } from './pages/estoque/reajuste-detalhe/reajuste-detalhe.component';
import { ContagemComponent } from './pages/estoque/contagem/contagem.component';
import { ContagemDetalheComponent } from './pages/estoque/contagem-detalhe/contagem-detalhe.component';
import { LancamentoPagarDetalheComponent } from './pages/financeiro/lancamento-pagar-detalhe/lancamento-pagar-detalhe.component';

import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';

import { CurrencyMaskModule } from 'ng2-currency-mask';
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from 'ng2-currency-mask/src/currency-mask.config';
import { LancamentoReceberComponent } from './pages/financeiro/lancamento-receber/lancamento-receber.component';
import { LancamentoReceberDetalheComponent } from './pages/financeiro/lancamento-receber-detalhe/lancamento-receber-detalhe.component';
import { ParcelaRecebimentoComponent } from './pages/financeiro/parcela-recebimento/parcela-recebimento.component';
import { ParcelaRecebimentoDetalheComponent } from './pages/financeiro/parcela-recebimento-detalhe/parcela-recebimento-detalhe.component';
import { JwtInterceptor } from './interceptor/jwt-interceptor';

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: 'right',
  allowNegative: true,
  decimal: ',',
  precision: 2,
  prefix: '',
  suffix: '',
  thousands: '.'
};

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AccordionModule,
    AutoCompleteModule,
    BreadcrumbModule,
    ButtonModule,
    CalendarModule,
    CarouselModule,
    CardModule,
    ChartModule,
    CheckboxModule,
    ChipsModule,
    CodeHighlighterModule,
    ColorPickerModule,
    ConfirmDialogModule,
    ContextMenuModule,
    DataScrollerModule,
    DataViewModule,
    DialogModule,
    DragDropModule,
    DropdownModule,
    EditorModule,
    FieldsetModule,
    FileUploadModule,
    GalleriaModule,
    GMapModule,
    GrowlModule,
    InplaceModule,
    InputMaskModule,
    InputSwitchModule,
    InputTextModule,
    InputTextareaModule,
    LightboxModule,
    ListboxModule,
    MegaMenuModule,
    MenuModule,
    MenubarModule,
    MessagesModule,
    MultiSelectModule,
    OrganizationChartModule,
    OrderListModule,
    OverlayPanelModule,
    PaginatorModule,
    PanelModule,
    PanelMenuModule,
    PasswordModule,
    PickListModule,
    ProgressBarModule,
    RadioButtonModule,
    RatingModule,
    ScheduleModule,
    SelectButtonModule,
    ScrollPanelModule,
    SlideMenuModule,
    SliderModule,
    SpinnerModule,
    SplitButtonModule,
    StepsModule,
    TableModule,
    TabMenuModule,
    TabViewModule,
    TerminalModule,
    TieredMenuModule,
    ToastModule,
    ToggleButtonModule,
    ToolbarModule,
    TooltipModule,
    TreeModule,
    TreeTableModule,
    AppRoutingModule,
    HttpClientModule,
    CurrencyMaskModule,
    NgxLoadingModule.forRoot({}),
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('token');
        },
        whitelistedDomains: ['localhost:8080'],
        throwNoTokenError: true
      }
    })
  ],
  declarations: [AppComponent, ArmazemComponent, ArmazemDetalheComponent,
    NaoEncontradoComponent, HomeComponent, LoginComponent, EmpresaComponent,
    EmpresaDetalheComponent, CargoComponent, CargoDetalheComponent, ColaboradorComponent,
    ColaboradorDetalheComponent, CompraRequisicaoComponent, CompraRequisicaoDetalheComponent,
    CompraCotacaoComponent, CompraCotacaoDetalheComponent, OrcamentoComponent,
    OrcamentoDetalheComponent, PedidoComponent, PedidoDetalheComponent,
    ReajusteComponent, ReajusteDetalheComponent, ContagemComponent, ContagemDetalheComponent,
    LancamentoPagarComponent, LancamentoPagarDetalheComponent, ParcelaPagamentoComponent,
    ParcelaPagamentoDetalheComponent, LancamentoReceberComponent, LancamentoReceberDetalheComponent,
    ParcelaRecebimentoComponent, ParcelaRecebimentoDetalheComponent],
  bootstrap: [AppComponent],
  providers: [MessageService, ConfirmationService, JwtHelperService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }]
})

export class AppModule { }
