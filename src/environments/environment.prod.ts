import { HttpHeaders } from '@angular/common/http';
export const environment = {
  production: true,
  enderecoUrl: '/',
  httpOpions: {
    headers: new HttpHeaders({
      'Content-type': 'application/json'
    })
  }
};
